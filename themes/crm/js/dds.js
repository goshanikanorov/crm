/**
 * Created by imac on 09.09.15.
 */
$(document).ready(function () {

    var container = document.getElementById('example');

    var diffRenderer = function (instance, td, row, col, prop, value, cellProperties) {
        Handsontable.renderers.TextRenderer.apply(this, arguments);
        //td.style.backgroundColor = '#c3f89c';
        //td.style.fontWeight = (col === 13 ? 'bold' : 'normal');
        td.style.fontWeight = 'bold';
    };
    var spaceRenderer = function (instance, td, row, col, prop, value, cellProperties) {
            Handsontable.renderers.TextRenderer.apply(this, arguments);
            td.style.backgroundColor = '#EEE';
            //td.style.fontWeight = (col === 13 ? 'bold' : 'normal');

        };

    var autosaveNotification;
    var exampleConsole = $('#saveAlert');

    var hot = new Handsontable(container, {
        data: data,
        sortIndicator: true,
        minSpareRows: 1,
        columnSorting: true,
        formulas: true,
        fixedColumnsLeft: 1,

        //contextMenu: true,
        //rowHeaders: true,
        colWidths: ['270', '80', '80', '80', '80', '80', '80', '80', '80', '80',
        '80', '80', '80', '130', '130', '130'],
        colHeaders: ['Статья', 'ЯНВАРЬ', 'ФЕВРАЛЬ', 'МАРТ', 'АПРЕЛЬ', 'МАЙ', 'ИЮНЬ', 'ИЮЛЬ', 'АВГУСТ',
        'СЕНТЯБРЬ', 'ОКТЯБРЬ','НОЯБРЬ','ДЕКАБРЬ', 'ФАКТ 2015', 'ПЛАН 2015', 'ОТКЛ'],
        columns: [
            {
                data: 1,
                readOnly: true,
                contextMenu: false
            },
            {data: 2},
            {data: 3},
            {data: 4},
            {data: 5},
            {data: 6},
            {data: 7},
            {data: 8},
            {data: 9},
            {data: 10},
            {data: 11},
            {data: 12},
            {data: 13},
            {data: 14},
            {data: 15},
            {data: 16}
        ],
        afterChange: function (change, source) {
        },
        /*cells: function (row, col, prop) {
             var cellProperties = {};
            if (row === 2 || row === 8) {
                cellProperties.renderer = diffRenderer;
            }
            else if (row === 3 || row === 9) {
                cellProperties.renderer = spaceRenderer;
            }
            return cellProperties;
        },
        mergeCells: [
              {row: 3, col: 0, rowspan: 1, colspan: 15},
              {row: 9, col: 0, rowspan: 1, colspan: 15}
        ]*/
    });

});


