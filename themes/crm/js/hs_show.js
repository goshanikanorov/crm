var container = document.getElementById('example');
var autosaveNotification;
var exampleConsole = $('#saveAlert');

var hot = new Handsontable(container, {
    data: data,
    sortIndicator: true,
    minSpareRows: 1,
    columnSorting: true,
    contextMenu: true,
    //rowHeaders: true,
    colWidths: ['40', '70', '100', '70', '70', '150', '200', '40','40', '300'],
    colHeaders: ['ID', 'Менеджер', 'Дата', 'Расход', 'Приход', 'Проект', 'Статья', 'Тип','Счет', 'Комментарий'],
    columns: [
        {},
        {
            editor: 'select',
            selectOptions: data_managers
        },
        {
            type: 'date',
            dateFormat: 'DD.MM.YYYY',
            correctFormat: true
        },
        {},
        {},
        {
            type: 'autocomplete',
            source: data_projects
        },
        {
            editor: 'select',
            selectOptions: data_types
        },
        {
            type: 'autocomplete',
            source: data_articles
        },
        {},
        {}
    ],

    afterChange: function (change, source) {

    //http://docs.handsontable.com/0.15.0-beta6/Hooks.html#event:afterChange
    //https://github.com/handsontable/handsontable/blob/master/demo/php/save.php

        console.log(source);

        if (source === 'loadData') {
            return; //don't save this change
        }
        $.each(change, function (index, value) {
            change[index][4] = data[change[index][0]][0];
        });
        clearTimeout(autosaveNotification);
        $.ajax({
            method: 'POST',
            url: 'index.php?r=cost/update',
            dataType : "JSON",
            //data: {data: hot.getData()} //все данные
            data: {data: change} //только изменения
        }).done(function (response) {
                    // alert(response);
                    hot.loadData(response);
                    data=response;
                    exampleConsole.text('Сохранено (' + change.length + ' ' + 'ячейка' + (change.length > 1 ? 's' : '') + ')').addClass('text-info').removeClass('text-muted');
                    autosaveNotification = setTimeout(function() {
                              exampleConsole.text('Изменения сохраняются автоматически').removeClass('text-info').addClass('text-muted');
                            }, 1000);
                });

        /*ajax('json/save.json', 'POST', JSON.stringify({data: change}), function (data) {
         exampleConsole.innerText  = 'Autosaved (' + change.length + ' ' + 'cell' + (change.length > 1 ? 's' : '') + ')';
         autosaveNotification = setTimeout(function() {
         exampleConsole.innerText ='Changes will be autosaved';
         }, 1000);
         });*/

    }
});


$('.dataTables-example').dataTable({
    responsive: true,
    "dom": 'T<"clear">lfrtip',
    "tableTools": {
        "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
    }
});

/* Init DataTables */
var oTable = $('#editable').dataTable();

/* Apply the jEditable handlers to the table */
oTable.$('td').editable( '../example_ajax.php', {
    "callback": function( sValue, y ) {
        var aPos = oTable.fnGetPosition( this );
        oTable.fnUpdate( sValue, aPos[0], aPos[1] );
    },
    "submitdata": function ( value, settings ) {
        return {
            "row_id": this.parentNode.getAttribute('id'),
            "column": oTable.fnGetPosition( this )[2]
        };
    },

    "width": "90%",
    "height": "100%"
} );


/* Datepicker select */
$('#data_5 .input-daterange').datepicker({
    format: 'dd.mm.yyyy',
    //keyboardNavigation: false,
    forceParse: false,
    autoclose: true,
    language: "ru",
    todayHighlight: true
    //startDate: '-3d'
});


$('#data_5 .input-daterange input[name="start"]').datepicker('setDate', '01.01.2015');
$('#data_5 .input-daterange input[name="end"]').datepicker('setDate', new Date());

/* Date BTN-GROUP click */
$('input[name="dateperiod"]').change(function () {

    $('#data_5 .input-daterange input[name="end"]').datepicker('setDate', new Date());
    var startDate = new Date();
    if ($(this).val() == 'month') {
        startDate.setMonth(startDate.getMonth() - 1);
        $('#data_5 .input-daterange input[name="start"]').datepicker('setDate', startDate);
    }
    else if ($(this).val() == 'quat') {
        startDate.setMonth(startDate.getMonth() - 3);
        $('#data_5 .input-daterange input[name="start"]').datepicker('setDate', startDate);
    }
    else if ($(this).val() == 'year') {
        startDate.setMonth(startDate.getMonth() - 12);
        $('#data_5 .input-daterange input[name="start"]').datepicker('setDate', startDate);
    }
});


$('#applyFilters').on('click', function(){
    var data = {
        datestart : $('#data_5 .input-daterange input[name="start"]').datepicker('getDate'),
        dateend : $('#data_5 .input-daterange input[name="end"]').datepicker('getDate'),
        manager : $('#selectManager').val(),
        project : $('#selectProject').val(),
        type    : $('#selectType').val(),
        item    : $('#selectItem').val()
    };

    $.ajax({
        method: 'POST',
        url: './index.php?r=cost/filter',
        dataType : "JSON",
        data: data//JSON.stringify(data)
    }).done(function (response) {
                console.log(response.data);
                hot.loadData(response.data);
            });

});

/* Chosen multiselect */
var config = {
    '.chosen-select'           : {},
    '.chosen-select-deselect'  : {allow_single_deselect:true},
    '.chosen-select-no-single' : {disable_search_threshold:10},
    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
    '.chosen-select-width'     : {width:"95%"}
}

for (var selector in config) {
    $(selector).chosen(config[selector]);
}



function fnClickAddRow() {
    $('#editable').dataTable().fnAddData( [
        "Custom row",
        "New row",
        "New row",
        "New row",
        "New row" ] );

}