<?php

$this->breadcrumbs = array(
    Yii::t('common', 'account_management')=>array('projecttotype/dashboard'),
    Yii::t('common', 'book_cost'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#cost-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('select_grid_item', "
function select_grid_item(element)
{
	console.log(element);
	$(element).replaceWith(\"<input type='text' value='\" + $(this).attr(\"href\") + \"'>\");
	parent.children(\":text\").focus();
	console.log('select');
}
",CClientScript::POS_HEAD);

Yii::app()->clientScript->registerScript('show', "
    var data=".$model->getHtJSON().";
    var data_managers=".$model->getJSON('manager').";
    var data_types=".$model->getJSON('types').";
    var data_projects=".$model->getJSON('projects').";
    var data_articles=".$model->getJSON('articles').";
    /*var data = [
      [1, 'Гоша', '25 мая', 2000, '','tsp', 'План', 'зарплата', 'Гоша зарплата октябрь 15'],
      [2, 'Сережа', '15 апр', '' , 1300, 'dachi', 'Факт', 'разработка', 'Тексты по новым страницам'],
      [3, 'Гоша', '18 дек', 12.3 , '', 'avesta', 'План', 'зарплата', 'Еще какой-то комментарий']
    ];*/
",CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/hs_show.js',CClientScript::POS_END);
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1><?php echo Yii::t('common', 'book_cost')?></h1>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		        // 'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
                'tagName'=>'ol',
				'htmlOptions'=>array('class'=>'breadcrumb'),
                'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                'inactiveLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                'separator'=>'',
		)); ?>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="ibox">
        <div class="ibox-content">
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group" id="data_5">
                <div class="input-daterange input-group" id="datepicker">
                    <input type="text" class="input-sm form-control" name="start" value=""/>
                    <span class="input-group-addon">по</span>
                    <input type="text" class="input-sm form-control" name="end" value="" />
                </div>
            </div>
        </div>
        <div class="col-sm-5 m-b-xs">
            <div data-toggle="buttons" class="btn-group">
                <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="dateperiod" value="month"> Месяц </label>
                <label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="dateperiod" value="quat"> Квартал </label>
                <label class="btn btn-sm btn-white active"> <input type="radio" id="option3" name="dateperiod" value="year"> Год </label>
            </div>
        </div>
        </div>
    <div class="row">
        <div class="col-sm-2 m-b-xs">
            <div class="form-group">
                <div class="input-group">
                    <? echo CHtml::dropDownList(
                        'manager',
                        'sex',
                        $model->getManagers(), 
                        array(
                            'class'=>'chosen-select',
                            'data-placeholder'=>'Менеджер',
                            'style'=>'width:150px;',
                            'id'=>'selectManager',
                            'multiple' => 'multiple',
                            'tabindex' => '4',
                        )
                    ); ?>
    <!--                 <select id="selectManager" data-placeholder="Менеджер" class="chosen-select" multiple
                            style="width:150px;" tabindex="4">
                        <option value="Гоша">Гоша</option>
                        <option value="Сережа">Сережа</option>
                    </select>
     -->            </div>
            </div>
        </div>
        <div class="col-sm-2 m-b-xs">
            <div class="form-group">
                <div class="input-group">
                    <? echo CHtml::dropDownList(
                        'project',
                        'numb',
                        $model->getProjects(), 
                        array(
                            'class'=>'chosen-select',
                            'data-placeholder'=>'Проект',
                            'style'=>'width:150px;',
                            'id'=>'selectProject',
                            'multiple' => 'multiple',
                            'tabindex' => '4',
                        )
                    ); ?>
    <!--                 <select id="selectProject" data-placeholder="Проект" class="chosen-select" multiple
                            style="width:150px;" tabindex="4">
                        <option value="">tsp</option>
                        <option value="Гоша">dachi</option>
                        <option value="Сережа">Энергосервис</option>
                    </select>
     -->            </div>
            </div>
        </div>
        <div class="col-sm-2 m-b-xs">
            <div class="form-group">
                <div class="input-group">
                    <? echo CHtml::dropDownList(
                        'types',
                        'numbtype',
                        $model->getTypes(), 
                        array(
                            'class'=>'chosen-select',
                            'data-placeholder'=>'Тип',
                            'style'=>'width:150px;',
                            'id'=>'selectType',
                            'multiple' => 'multiple',
                            'tabindex' => '4',
                        )
                    ); ?>
    <!--                 <select id="selectType" data-placeholder="Тип" class="chosen-select" multiple
                            style="width:150px;" tabindex="4">
                        <option value="">tsp</option>
                        <option value="Гоша">dachi</option>
                        <option value="Сережа">Энергосервис</option>
                    </select>
     -->            </div>
            </div>
        </div>
        <div class="col-sm-2 m-b-xs">
            <div class="form-group ">
                <div class="input-group">
                    <? echo CHtml::dropDownList(
                        'article',
                        'numbarticle',
                        $model->getArticles(), 
                        array(
                            'class'=>'chosen-select',
                            'data-placeholder'=>'Статья',
                            'style'=>'width:150px;',
                            'id'=>'selectItem',
                            'multiple' => 'multiple',
                            'tabindex' => '4',
                        )
                    ); ?>
    <!--                 <select id="selectItem" data-placeholder="Статья" class="chosen-select" multiple
                            style="width:150px;" tabindex="4">
                        <option value="">tsp</option>
                        <option value="Гоша">dachi</option>
                        <option value="Сережа">Энергосервис</option>
                    </select>
     -->            </div>
            </div>
        </div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-primary" id="applyFilters">
                Показать!
            </button>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6" ><p id="saveAlert" class="text-muted">Изменения сохраняются автоматически</p></div>
    </div>
    <div class="row">
        <div class="col-md-12"><div id="example"></div></div>
    </div>
    </div>
</div>
</div>
</div>