<?

$this->breadcrumbs=array(
		Yii::t('common', 'manage_cost'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#cost-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('select_grid_item', "
function select_grid_item(element)
{
	console.log(element);
	$(element).replaceWith(\"<input type='text' value='\" + $(this).attr(\"href\") + \"'>\");
	parent.children(\":text\").focus();
	console.log('select');
}
",CClientScript::POS_HEAD);?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1><?php echo Yii::t('common', 'manage_cost')?></h1>
		
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		        'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
				'htmlOptions'=>array ('class'=>'breadcrumb'),
		)); ?>
	</div>
</div>

<?php /* 
 * В гриде CSS-классы бутстрапа вынес в конфиг приложения.
 * */ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cost-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=> $columns,
)); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('class'=> 'form-horizontal')
)); ?>

<h1><?php echo Yii::t('common', 'cost_add')?></h1>
 <div class="form-group">
 	 <?php echo $form->labelEx($model, 'project_id', array('class'=>'col-lg-2 control-label')); ?>
     <div class="col-lg-10">
         <?php echo $form->dropDownList($model, 'project_id', Project::model()->getListProject(), array('class'=> 'form-control', 'empty'=>'')); ?>
     </div>
 </div>
 <div class="form-group">
 	 <?php echo $form->labelEx($model, 'type', array('class'=>'col-lg-2 control-label')); ?>
     <div class="col-lg-10">
         <?php echo $form->dropDownList($model, 'type', CostType::model()->getListType(), array('class'=> 'form-control', 'empty'=>'')); ?>
     </div>
 </div>
 <div class="form-group">
 	 <?php echo $form->labelEx($model, 'user_id', array('class'=>'col-lg-2 control-label')); ?>
     <div class="col-lg-10">
         <?php echo $form->dropDownList($model, 'user_id', User::model()->getListUser(), array('class'=> 'form-control', 'empty'=>'')); ?>
     </div>
 </div>
 <div class="form-group">
 	 <?php echo $form->labelEx($model, 'article', array('class'=>'col-lg-2 control-label')); ?>
     <div class="col-lg-10">
         <?php echo $form->dropDownList($model, 'article', CostArticle::model()->getListArticle(), array('class'=> 'form-control', 'empty'=>'')); ?>
     </div>
 </div>
 <div class="form-group">
 	 <?php echo $form->labelEx($model, 'sum', array('class'=>'col-lg-2 control-label')); ?>
     <div class="col-lg-10">
         <?php echo $form->textField($model,'sum', array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
     </div>
 </div>
 <div class="form-group">
 	 <?php echo $form->labelEx($model, 'comment', array('class'=>'col-lg-2 control-label')); ?>
     <div class="col-lg-10">
         <?php echo $form->textField($model,'comment', array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
     </div>
 </div>
 <div class="form-group">
     <?php echo $form->labelEx($model, 'date', array('class'=>'col-lg-2 control-label')); ?>
     <div class="col-lg-10">
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'end_date',    
            'value'=>date('d/m/Y'),
            'options'=>array(        
                'showButtonPanel'=>true,
                'dateFormat'=>'mm/dd/yy',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
            ),
            'htmlOptions'=>array(
                'style'=>''
            ),
        ));
        ?>
     </div>
 </div>
 <div class="form-group">
     <div class="col-lg-offset-2 col-lg-10">
         <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('admin', 'create') : Yii::t('admin', 'save'), array('class' => 'btn btn-sm btn-white')); ?>
     </div>
 </div>	        			

<?php $this->endWidget(); ?>