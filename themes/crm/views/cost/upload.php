<h1>Загрузка Excel-файла</h1>

<div class="widget">
    <div class="body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array(
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ),
        ));
        ?>
        
        <?php
        if ($status !== null):
            $htmlOptions = array(
                'class' => 'alert alert-danger',
            );
            echo CHtml::tag('div', $htmlOptions, $status, true);
        endif;
        ?>

        <fieldset>
            <div class="control-group">
                <label class="control-label">Файл для загрузки</label>
                <div class="controls form-group fileupload-buttonbar">
                    <p>
                        Файлы с расширениями: *.xls / *.xlsx
                        <br>
                        Пример структуры:
                        <br>
                        <?= CHtml::image(Yii::app()->request->baseUrl . '/static/css/img/xls-template.png', '', array()); ?>
                    </p>
                    <?php
                    $mimes = implode(',', Cost::getExcelMimeTypes());
                    echo CHtml::fileField('FlatExcel', '', array(
                        'accept' => $mimes,
                    ));
                    ?>
                </div>
            </div>
        </fieldset>

        <div class="form-actions">
            <?php echo CHtml::submitButton('Загрузить', array('class' => 'btn btn-success')); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>