<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>

    <link href="<?=Yii::app()->theme->baseUrl?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?=Yii::app()->theme->baseUrl?>/css/animate.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/css/style.css" rel="stylesheet">
	<script src="<?=Yii::app()->theme->baseUrl?>/js/jquery-2.1.1.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/bootstrap.min.js"></script>
</head>

<body class="gray-bg">           	
	<?php echo $content; ?> 
	<!-- Mainly scripts -->
</body>

</html>