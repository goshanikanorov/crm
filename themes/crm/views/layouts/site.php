<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRM</title>
    <link href="<?=Yii::app()->theme->baseUrl?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/css/animate.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/css/style.css" rel="stylesheet">
    
    <!-- Mainly scripts -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/jquery-2.1.1.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/bootstrap.min.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
</head>

<body class="top-navigation">
	<div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
			!!!!
        
        	<!-- HEADER -->
	        <div class="row border-bottom white-bg">
		        <nav class="navbar navbar-static-top" role="navigation">
		            <div class="navbar-header">
		                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
		                    <i class="fa fa-reorder"></i>
		                </button>
		                <a href="?r=site/index" class="navbar-brand"><?php echo Yii::t('common', 'crm');?></a>
		            </div>
		            <div class="navbar-collapse collapse" id="navbar">
		                <ul class="nav navbar-nav">
		                    <li class="active">
		                        <a aria-expanded="false" role="button" href="?r=site/index"><?php echo Yii::t('common', 'home');?></a>
		                    </li>
		                    <!--<li class="dropdown">
		                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Menu item <span class="caret"></span></a>
		                        <ul role="menu" class="dropdown-menu">
		                            <li><a href="">Menu item</a></li>
		                            <li><a href="">Menu item</a></li>
		                            <li><a href="">Menu item</a></li>
		                            <li><a href="">Menu item</a></li>
		                        </ul>
		                    </li>-->
		                </ul>
		                <?php if(!Yii::app()->user->isGuest):?>
		                <ul class="nav navbar-top-links navbar-right">
		                    <li>
		                        <a href="?r=site/logout">
		                            <i class="fa fa-sign-out"></i> <?php echo Yii::t('common', 'log_out');?>
		                        </a>
		                    </li>
		                </ul>
		                <?php endif;?>
		            </div>
		        </nav>
	        </div>

	        <!-- CONTENT -->
	         <div class="wrapper wrapper-content">
	            <div class="container">             	
			   		 <?php echo $content; ?> 
		   		 </div>
	   		 </div>
    
    		<!-- FOOTER -->
		    <div class="footer">
		        <div class="pull-right">
		            <!-- footer content -->
		        </div>
		        <div>
		            <strong>Copyright</strong> Example Company &copy; 2014-2015
		        </div>
		    </div>
		 </div>
	</div>     
</body>
</html>