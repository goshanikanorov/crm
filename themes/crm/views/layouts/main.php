<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Dashboard v.4</title>

    <link href="<?=Yii::app()->theme->baseUrl?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Morris -->
    <link href="<?=Yii::app()->theme->baseUrl?>/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <link href="<?=Yii::app()->theme->baseUrl?>/css/animate.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/css/style.css" rel="stylesheet">

    <link href="<?=Yii::app()->theme->baseUrl?>/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/css/handsontable.full.css" rel="stylesheet">

    <link href="<?=Yii::app()->theme->baseUrl?>/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="<?=Yii::app()->theme->baseUrl?>/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">


    	<!-- Mainly scripts -->
    <script>if (!window.jQuery) {
    	document.write('<script src="<?=Yii::app()->theme->baseUrl?>\/js\/jquery-2.1.1.js"><\/script>');
    }
    </script>    
    <script src="<?=Yii::app()->theme->baseUrl?>/js/bootstrap.min.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

	<script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/jeditable/jquery.jeditable.js"></script>

    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/dataTables/dataTables.responsive.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/dataTables/dataTables.tableTools.min.js"></script>

    <!-- Flot -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/flot/curvedLines.js"></script>

    <!-- Peity -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/inspinia.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- Sparkline -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/chartJs/Chart.min.js"></script>

    <!-- DatePicker-->
	<script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	
	<!-- Chosen-->
	<script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/chosen/chosen.jquery.js"></script>

	<!-- Handsontable-->
    <script src="<?=Yii::app()->theme->baseUrl?>/js/handsontable.full.js"></script>

   <!-- <script>
        $(document).ready(function() {


            var d1 = [[1262304000000, 6], [1264982400000, 3057], [1267401600000, 20434], [1270080000000, 31982], [1272672000000, 26602], [1275350400000, 27826], [1277942400000, 24302], [1280620800000, 24237], [1283299200000, 21004], [1285891200000, 12144], [1288569600000, 10577], [1291161600000, 10295]];
            var d2 = [[1262304000000, 5], [1264982400000, 200], [1267401600000, 1605], [1270080000000, 6129], [1272672000000, 11643], [1275350400000, 19055], [1277942400000, 30062], [1280620800000, 39197], [1283299200000, 37000], [1285891200000, 27000], [1288569600000, 21000], [1291161600000, 17000]];

            var data1 = [
                { label: "Data 1", data: d1, color: '#17a084'},
                { label: "Data 2", data: d2, color: '#127e68' }
            ];
            $.plot($("#flot-chart1"), data1, {
                xaxis: {
                    tickDecimals: 0
                },
                series: {
                    lines: {
                        show: true,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 1
                            }, {
                                opacity: 1
                            }]
                        },
                    },
                    points: {
                        width: 0.1,
                        show: false
                    },
                },
                grid: {
                    show: false,
                    borderWidth: 0
                },
                legend: {
                    show: false,
                }
            });

            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [65, 59, 40, 51, 36, 25, 40]
                    },
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: [48, 48, 60, 39, 56, 37, 30]
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        });
    </script>  --> 
</head>

<body>
    <div id="wrapper">
    
    	<!-- SIDEBAR NAV -->
    	
    	<nav class="navbar-default navbar-static-side" role="navigation">
       		<div class="sidebar-collapse">
       			<?php $this->widget('zii.widgets.CMenu', array(
	            	'htmlOptions'=>array('id' => 'side-menu', 'class' => 'nav'),
	            	'encodeLabel'=>false,
				    'items'=>array(		
				    	array(
				    		'label'=>'<div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="'.Yii::app()->theme->baseUrl.'/img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">'.Yii::app()->user->name.'</strong>
                             </span> <span class="text-muted text-xs block">Список действий <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
                            <li><a href="login.html">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
',
				    		// 'url'=>array('admin/index'), 'visible'=>Yii::app()->user->isAdmin(),
				    		'itemOptions'=>array('class'=>'nav-header')
				    		),
						// array('label'=>Yii::t('admin', 'main'), 'url'=>array('admin/index'), 'visible'=>Yii::app()->user->isAdmin()),	        
				        array('label'=>Yii::t('admin', 'users'), 'url'=>array('user/admin'), 'visible'=>Yii::app()->user->isAdmin(), 'active'=>Yii::app()->controller->id=='user'),
						array('label'=>Yii::t('common', 'manage_clients'), 'url'=>array('project/admin'), 'visible'=> !Yii::app()->user->isGuest, 'active'=>Yii::app()->controller->id=='project'),
						array('label'=>Yii::t('common', 'manage_cost'), 'url'=>array('projectToType/dashboard'), 'visible'=> !Yii::app()->user->isGuest, 'active'=>Yii::app()->controller->id=='projectToType'),
				    ),
				));?>
			</div>
	    </nav>
    
	   <div id="page-wrapper" class="gray-bg">    
	       
	        <!-- HEADER -->
	        
	        <div class="row border-bottom">
		        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
		        <div class="navbar-header">
		            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
		        </div>
		            <ul class="nav navbar-top-links navbar-right">
		                <li class="dropdown">
		                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
		                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
		                    </a>
		                    <ul class="dropdown-menu dropdown-messages">
		                        <li>
		                            <div class="dropdown-messages-box">
		                                <a href="profile.html" class="pull-left">
		                                    <img alt="image" class="img-circle" src="<?=Yii::app()->theme->baseUrl?>/img/a7.jpg">
		                                </a>
		                                <div>
		                                    <small class="pull-right">46h ago</small>
		                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
		                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
		                                </div>
		                            </div>
		                        </li>
		                        <li class="divider"></li>
		                        <li>
		                            <div class="dropdown-messages-box">
		                                <a href="profile.html" class="pull-left">
		                                    <img alt="image" class="img-circle" src="<?=Yii::app()->theme->baseUrl?>/img/a4.jpg">
		                                </a>
		                                <div>
		                                    <small class="pull-right text-navy">5h ago</small>
		                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
		                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
		                                </div>
		                            </div>
		                        </li>
		                        <li class="divider"></li>
		                        <li>
		                            <div class="dropdown-messages-box">
		                                <a href="profile.html" class="pull-left">
		                                    <img alt="image" class="img-circle" src="<?=Yii::app()->theme->baseUrl?>/img/profile.jpg">
		                                </a>
		                                <div>
		                                    <small class="pull-right">23h ago</small>
		                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
		                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
		                                </div>
		                            </div>
		                        </li>
		                        <li class="divider"></li>
		                        <li>
		                            <div class="text-center link-block">
		                                <a href="mailbox.html">
		                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
		                                </a>
		                            </div>
		                        </li>
		                    </ul>
		                </li>
		                <li class="dropdown">
		                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
		                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
		                    </a>
		                    <ul class="dropdown-menu dropdown-alerts">
		                        <li>
		                            <a href="mailbox.html">
		                                <div>
		                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
		                                    <span class="pull-right text-muted small">4 minutes ago</span>
		                                </div>
		                            </a>
		                        </li>
		                        <li class="divider"></li>
		                        <li>
		                            <a href="profile.html">
		                                <div>
		                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
		                                    <span class="pull-right text-muted small">12 minutes ago</span>
		                                </div>
		                            </a>
		                        </li>
		                        <li class="divider"></li>
		                        <li>
		                            <a href="grid_options.html">
		                                <div>
		                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
		                                    <span class="pull-right text-muted small">4 minutes ago</span>
		                                </div>
		                            </a>
		                        </li>
		                        <li class="divider"></li>
		                        <li>
		                            <div class="text-center link-block">
		                                <a href="notifications.html">
		                                    <strong>See All Alerts</strong>
		                                    <i class="fa fa-angle-right"></i>
		                                </a>
		                            </div>
		                        </li>
		                    </ul>
		                </li>
		                <li>
		                    <a href="?r=site/logout">
		                        <i class="fa fa-sign-out"></i> <?php echo Yii::t('common', 'log_out');?>
		                    </a>
		                </li>
		            </ul>
		        </nav>
	        </div>
	        <!-- CONTENT -->
			<?php echo $content;?>   
	    	<!-- FOOTER -->
		    <div class="footer">
		        <div class="pull-right">
		            <!-- 10GB of <strong>250GB</strong> Free. -->
		        </div>
		        <div>
		            <strong>Copyright</strong> Strikt Web Solutions &copy; 2014-2015
		        </div>
		    </div>
		 </div>
	</div>  
</body>
</html>