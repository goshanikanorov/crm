<?php

$this->breadcrumbs = array(
    Yii::t('common', 'account_management'),
    Yii::t('common', 'book_cost'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#cost-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
Yii::app()->clientScript->registerScript('select_grid_item', "
function select_grid_item(element)
{
	console.log(element);
	$(element).replaceWith(\"<input type='text' value='\" + $(this).attr(\"href\") + \"'>\");
	parent.children(\":text\").focus();
	console.log('select');
}
",CClientScript::POS_HEAD);

Yii::app()->clientScript->registerScript('show', "


   /* var data = [
          ['Сумма договора', 10000, 20000, 30000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000],
          ['Комиссия', 10000, , 30000, 10000, 10000, , 10000, 10000, 10000, , 10000, ],
          ['ИТОГО ВЫРУЧКА'],
          [],
          ['Субподряд дизайн', 10000, 20000, 30000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000],
          ['- Оплата субподрядчику', 10000, 20000, 30000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000],
          ['-- Комиссия за перевод', 10000, 20000, 30000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000],
          ['Субподряд верстка', 10000, 20000, 30000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000],
          ['ИТОГО РАСХОДОВ', 10000, 20000, 30000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000],
          [],
          ['МАРЖ. ДОХОД', 10000, 20000, 30000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000],


        ];*/

        var data = $dds;
",CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/dds.js',CClientScript::POS_END);
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1>Бюджет договора №<?php //echo $model->projectType->id ?> <?php //echo $model->project->name ?> <?php //echo $model->projectType->name ?> </h1>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		        // 'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
                'tagName'=>'ol',
				'htmlOptions'=>array('class'=>'breadcrumb'),
                'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                'inactiveLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                'separator'=>'',
		)); ?>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3">
                        <h3>Год: <?= $year ?></h3>
                    </div>
                    <div class="col-sm-9" style="padding-bottom: 20px;">
                        <button type="button" class="btn btn-primary pull-right" id="applyFilters">
                            Редактировать данные
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="overflow: auto; height: 900px;width:1292px; padding-right: 0px;">

                        <div id="example" ></div>
                    </div>
                </div>
            </div>
</div>
</div>
</div>