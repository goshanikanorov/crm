<?php
/* @var $this ProjectToTypeController */
/* @var $model ProjectToType */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_type_id'); ?>
		<?php echo $form->textField($model,'project_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_status_id'); ?>
		<?php echo $form->textField($model,'project_status_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_changed_date'); ?>
		<?php echo $form->textField($model,'last_changed_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->