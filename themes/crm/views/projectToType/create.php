<?php
/* @var $this ProjectToTypeController */
/* @var $model ProjectToType */

$this->breadcrumbs=array(
	'Project To Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProjectToType', 'url'=>array('index')),
	array('label'=>'Manage ProjectToType', 'url'=>array('admin')),
);
?>

<h1>Create ProjectToType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>