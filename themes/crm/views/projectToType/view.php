<?php
/* @var $this ProjectToTypeController */
/* @var $model ProjectToType */

$this->breadcrumbs=array(
	'Project To Types'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProjectToType', 'url'=>array('index')),
	array('label'=>'Create ProjectToType', 'url'=>array('create')),
	array('label'=>'Update ProjectToType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProjectToType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProjectToType', 'url'=>array('admin')),
);
?>

<h1>View ProjectToType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'project_type_id',
		'project_id',
		'project_status_id',
		'last_changed_date',
	),
)); ?>
