<?php $this->breadcrumbs = array(
    Yii::t('common', 'account_management'),
);
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1>Управленческий учет</h1>

        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
        		        'links'=>$this->breadcrumbs,
        		        // 'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
                        'tagName'=>'ol',
        				'htmlOptions'=>array('class'=>'breadcrumb'),
                        'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                        'inactiveLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                        'separator'=>'',
        		)); ?>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#">Список всех клиентов</a>
                        <br>
                        <?php echo CHtml::link('Бюджеты договоров',array('projectToType/admin')); ?>
                        <br>
                        <?php echo CHtml::link('Книга учета',array('cost/admin')); ?>
                        <br>
                        <?php echo CHtml::link('Движение денежных средств',array('projectToType/dds')); ?>
                        <br>
                        <?php echo CHtml::link('Загрузка Excel',array('cost/upload')); ?>
                        <br>
                        <?php echo ($role=='seo'?CHtml::link('Страница SEO',array('seo/view')):''); ?>
                    </div>
                </div>
                <div class="row">
                </div>
            </div>
</div>
</div>
</div>