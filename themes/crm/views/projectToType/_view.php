<?php
/* @var $this ProjectToTypeController */
/* @var $data ProjectToType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('project_status_id')); ?>:</b>
	<?php echo CHtml::encode($data->project_status_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_changed_date')); ?>:</b>
	<?php echo CHtml::encode($data->last_changed_date); ?>
	<br />


</div>