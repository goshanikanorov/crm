<?php
/* @var $this ProjectToTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Project To Types',
);

$this->menu=array(
	array('label'=>'Create ProjectToType', 'url'=>array('create')),
	array('label'=>'Manage ProjectToType', 'url'=>array('admin')),
);
?>

<h1>Project To Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
