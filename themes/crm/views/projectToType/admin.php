<?php
/* @var $this ProjectToTypeController */
/* @var $model ProjectToType */

$this->breadcrumbs = array(
    'Project To Types' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List ProjectToType', 'url' => array('index')),
    array('label' => 'Create ProjectToType', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#project-to-type-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h1>Список всех договоров</h1>
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
            // 'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
            'tagName' => 'ol',
            'htmlOptions' => array('class' => 'breadcrumb'),
            'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
            'inactiveLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
            'separator' => '',
        ));
        ?>
    </div>
</div>

<?php /* 
 * В гриде CSS-классы бутстрапа вынес в конфиг приложения.
 * Колонка с кнопками действия использует не дефолтный класс, а свой с иконками
 * Сделал проверку для project_type_id и project_id на существование связи
 * Можно сделать геттер, но так тоже норм
 * */ ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'project-to-type-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'columns' => array(
                        array('class' => 'CButtonColumn',
                            'template' => '{budget}',
                            'buttons' => array(
                                'budget' => array(
                                    'label' => 'Бюджет',
                                    'url' => 'Yii::app()->createURL("projectToType/budget", array("id"=> $data->id))',
                                )
                            )
                        ),
                        'id',
                        array(
                            'name' => 'project_type_id',
                            'value' => 'isset($data->projectType) ? $data->projectType->name : ""',
                        ),
                        array(
                            'name' => 'project_id',
                            'value' => 'isset($data->project) ? $data->project->name : ""',
                        ),
                        'project_status_id',
                        'last_changed_date',
                        array(
                            'class' => 'ButtonColumn',
                        ),
                    ),
                ));
                ?>

                <?php var_dump($model->projectType) ?>

            </div>
        </div>
    </div>
</div>