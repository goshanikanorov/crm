<?php
/* @var $this ProjectToTypeController */
/* @var $model ProjectToType */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-to-type-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'project_type_id'); ?>
		<?php echo $form->textField($model,'project_type_id'); ?>
		<?php echo $form->error($model,'project_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'project_id'); ?>
		<?php echo $form->textField($model,'project_id'); ?>
		<?php echo $form->error($model,'project_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'project_status_id'); ?>
		<?php echo $form->textField($model,'project_status_id'); ?>
		<?php echo $form->error($model,'project_status_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_changed_date'); ?>
		<?php echo $form->textField($model,'last_changed_date'); ?>
		<?php echo $form->error($model,'last_changed_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->