<?php
/* @var $this ProjectToTypeController */
/* @var $model ProjectToType */

$this->breadcrumbs=array(
	'Project To Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProjectToType', 'url'=>array('index')),
	array('label'=>'Create ProjectToType', 'url'=>array('create')),
	array('label'=>'View ProjectToType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectToType', 'url'=>array('admin')),
);
?>

<h1>Update ProjectToType <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>