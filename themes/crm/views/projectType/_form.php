<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'project-type-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,

)); ?>

	<!--<p class="note">Поля со <span class="required">*</span> обязательны для заполнения.</p>-->

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
			</div>

		</div>
	</div>

	<h2>Статьи для договора</h2>
	<div class="row">
		<div class="col-md-6">

			<?php ?>
			<table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Название</th>
					<th>Тип</th>
					<th>Подстатья у</th>
					<th></th>
				</tr>

				</thead>
				<tbody>


				<?php
				foreach ($model->costTypes as $id => $costType)
				{
					echo '<tr>
	                      <td>'.$costType->id.'</td>
	                      <td>'.$costType->type_name.'</td>
	                      <td>'.$costType->getCostOptionLabel().'</td>
	                      <td>'.$costType->parentName.'</td>
	                      <td><button class="btn btn-xs btn-danger fa fa-times" type="button"></button></td>
	                      </tr>';
				}
				?>
				<tr class="clone-fields">
					<td></td>
					<td><?= CHtml::textField('costTypes[a1][type_name]', '', array('class' => 'form-control m-b')); ?></td>
					<td><?= CHtml::dropDownList('costTypes[a1][type_pf]','wtf',CostType::model()->getCostOptions(), array('class'=>'form-control m-b')); ?></td>
					<td><?= CHtml::dropDownList('costTypes[a1][parent]','wtf',CHtml::listData(CostType::model()->findAll(),'id','type_name'), array('class'=>'form-control m-b', 'empty'=>'-')); ?></td>
					<td></td>
				</tr>
				</tbody>
			</table>
			<?php
			//$options = array('class' => 'btn btn-default btn-xs pull-right', 'id'=>'add-cost');
			//echo CHtml::tag('button', $options, 'Добавить статью', true);
			?>

			<div>
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', array('class'=>'btn btn-sm btn-primary pull-right')); ?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>