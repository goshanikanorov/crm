<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */

$this->breadcrumbs=array(
	'Управленческий учет'=>array('index'),
	'Договор "'.$model->name.'"'=>array('view','id'=>$model->id),
	'Изменить',
);

$this->menu=array(
	array('label'=>'List ProjectType', 'url'=>array('index')),
	array('label'=>'Create ProjectType', 'url'=>array('create')),
	array('label'=>'View ProjectType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProjectType', 'url'=>array('admin')),
);
?>
<script>
    $(document).ready(function () {

        $('#add-cost').on('click', function () {

        });

    });
</script>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h1>Редактирование договора "<?php echo $model->name; ?>"</h1>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			// 'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
			'tagName'=>'ol',
			'htmlOptions'=>array('class'=>'breadcrumb'),
			'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
			'inactiveLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
			'separator'=>'',
		)); ?>
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="ibox">
			<div class="ibox-content">
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>





			</div>
		</div>
	</div>
</div>

