<?php
/* @var $this ProjectTypeController */
/* @var $model ProjectType */

$this->breadcrumbs = array(
    'Project Types' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List ProjectType', 'url' => array('index')),
    array('label' => 'Create ProjectType', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#cost-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h1>Типы договоров</h1>
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
            // 'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
            'tagName' => 'ol',
            'htmlOptions' => array('class' => 'breadcrumb'),
            'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
            'inactiveLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
            'separator' => '',
        ));
        ?>
    </div>
</div>

<?php /* 
 * В гриде CSS-классы бутстрапа вынес в конфиг приложения.
 * Колонка с кнопками действия использует не дефолтный класс, а свой с иконками
 * */ ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'project-type-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'columns' => array(
                        'id',
                        'name',
                        array(
                            'class' => 'ButtonColumn',
                        ),
                    ),
                ));
                ?>

            </div>
        </div>
    </div>
</div>
