<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    Yii::t('admin', 'manage_users'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h1><?php echo Yii::t('admin', 'manage_users') ?></h1>

        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
            'homeLink' => CHtml::link(Yii::t('common', 'home'), '/'),
            'htmlOptions' => array('class' => 'breadcrumb'),
        ));
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins"> 
            <div class="ibox-title">
                <?php /* echo CHtml::link(Yii::t('admin', 'advanced_search'),'#', array('class'=>'search-button')); */ ?>
                <h5><?php echo Yii::t('admin', 'advanced_search'); ?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>                             
                </div>
            </div>                   
            <div class="ibox-content" style="display:none;">
                <div class="search-form">
                    <?php
                    $this->renderPartial('_search', array(
                        'model' => $model,
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>	
</div>
<!-- search-form -->

<?php /* 
 * В гриде CSS-классы бутстрапа вынес в конфиг приложения.
 * Колонка с кнопками действия использует не дефолтный класс, а свой с иконками
 * */ ?>

<a href="<?php echo Yii::app()->createUrl('user/create') ?>"><?php echo Yii::t('admin', 'new_user') ?></a>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'user-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'login',
        'email',
        'name',
        'surname',
        'middlename',
        array(
            'name' => 'role',
            'value' => '$data->getRolesText()',
        ),
        'phone',
        'create_date',
        array(
            'class' => 'ButtonColumn',
            'buttons' => array(
                'delete' => array(
                    'click' => "function(){
                        $.ajax({
                        type:'POST',
                        url:$(this).attr('href'),
                        success:function(data) {
                            $.fn.yiiGridView.update('user-grid');
							$('#user-grid').yiiGridView('update', {
								data: $(this).serialize()
							});
                        }
                    })
                    return false;
                    }",
                ),
            )
        ),
    ),
));
