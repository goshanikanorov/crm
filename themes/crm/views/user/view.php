<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
		Yii::t('admin', 'manage_users') => array('admin'),
		$model->name,
);

?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1><?php echo Yii::t('admin', 'view_user')." ".$model->name; ?></h1>
		
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		                    'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
		)); ?>
	</div>
</div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'login',
		'email',
		'name',
		'surname',
		'middlename',
		array('label' => Yii::t('admin', 'role'), 'type' => 'raw', 'value' => $model->getRolesText()),
		'phone',
		'create_date',
	),
)); ?>
