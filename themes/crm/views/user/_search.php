<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions' => array('class'=> 'form-horizontal')
)); ?>

	<!-- <div class="form-group">
        <?php echo $form->label($model,'id', array('class'=>'col-lg-2 control-label') ); ?>
        <div class="col-lg-8">
	        <?php echo $form->textField($model,'id',array('class'=> 'form-control')); ?>
	    </div>
   </div>-->	

	<div class="form-group">
		<?php echo $form->label($model,'login', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->textField($model,'login',array('size'=>50,'maxlength'=>20, 'class'=> 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'email', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		</div>		
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'name', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'surname', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->textField($model,'surname',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'middlename', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->textField($model,'middlename',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'role', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->dropDownList($model, 'role', User::getRoles(), array('class'=> 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'phone', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'create_date', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-8">
			<?php echo $form->textField($model,'create_date', array('class'=> 'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
        <div class="col-lg-offset-2 col-lg-10">
			<?php echo CHtml::submitButton(Yii::t('admin', 'search'), array('class' => 'btn btn-sm btn-white')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>