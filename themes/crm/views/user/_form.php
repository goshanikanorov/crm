 <?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins"> 
			<div class="ibox-title">
				<p class="note"><?php echo Yii::t('admin', 'fields_with')?> <span class="required">*</span> <?php echo Yii::t('admin', 'are_required')?>.</p>
			</div>                   
        	<div class="ibox-content">
        		
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'user-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
					'htmlOptions' => array('class'=> 'form-horizontal')
								)); ?>
					<?php if($this->error):?><div class="alert alert-danger"><?php echo $this->error; ?></div><?php endif;?>
					<?php if($form->errorSummary($model) != ""):?><div class="alert alert-danger"><?php echo $form->errorSummary($model); ?></div><?php endif;?>

                     <div class="form-group">
                     	 <?php echo $form->labelEx($model,'login', array('class'=>'col-lg-2 control-label') ); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->textField($model,'login',array('size'=>50,'maxlength'=>20, 'class'=> 'form-control')); ?>
                         </div>
                     </div>
                     
                     <div class="form-group">
                     	 <?php echo $form->labelEx($model,'password', array('class'=>'col-lg-2 control-label')); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->passwordField($model,'password', array('size'=>50,'maxlength'=>20, 'value'=> '', 'class'=> 'form-control')); ?>
                         </div>
                     </div>
                     
                     <div class="form-group">
                     	 <?php echo $form->labelEx($model,'email', array('class'=>'col-lg-2 control-label') ); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->emailField($model,'email',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
                         </div>
                     </div>
                     
                     <div class="form-group">
                     	 <?php echo $form->labelEx($model,'name', array('class'=>'col-lg-2 control-label') ); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
                         </div>
                     </div>
                     
                      <div class="form-group">
                     	 <?php echo $form->labelEx($model,'surname', array('class'=>'col-lg-2 control-label') ); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->textField($model,'surname',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
                         </div>
                     </div>
                     
                      <div class="form-group">
                     	 <?php echo $form->labelEx($model,'middlename', array('class'=>'col-lg-2 control-label') ); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->textField($model,'middlename',array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
                         </div>
                     </div>
                     
                     <div class="form-group">
                     	 <?php echo $form->labelEx($model,'role', array('class'=>'col-lg-2 control-label') ); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->dropDownList($model, 'role', User::getRoles(), array('class'=> 'form-control')); ?>           
                         </div>
                     </div>
                     
                     <div class="form-group">
                     	 <?php echo $form->labelEx($model,'phone', array('class'=>'col-lg-2 control-label') ); ?>
                         <div class="col-lg-10">
	                         <?php echo CHtml::activeTelField($model,'phone',array('class'=> 'form-control', 'data-mask'=>'+7(999) 999-9999')); ?>
                         </div>
                     </div>
                                
                     <div class="form-group">
                         <div class="col-lg-offset-2 col-lg-10">
                             <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('admin', 'create') : Yii::t('admin', 'save'), array('class' => 'btn btn-sm btn-white')); ?>
                         </div>
                     </div>	        			
				<?php $this->endWidget(); ?>
			</div>
   		</div>
	</div>	
</div>
<style>
	#user-form label.error{margin-left:0;}				
</style>