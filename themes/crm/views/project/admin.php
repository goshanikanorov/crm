<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
		Yii::t('common', 'manage_clients'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#project-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");?>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1><?php echo Yii::t('common', 'manage_clients')?></h1>
		
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		        'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
				'htmlOptions'=>array ('class'=>'breadcrumb'),
		)); ?>
	</div>
</div>

<?php if(Yii::app()->user->isAdmin()):?>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins"> 
			<div class="ibox-title">
				<?php /*echo CHtml::link(Yii::t('admin', 'advanced_search'),'#', array('class'=>'search-button')); */?>
				<h5><?php echo Yii::t('admin', 'advanced_search');?></h5>
				<div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>                             
                </div>
			</div>                   
        	<div class="ibox-content" style="display:none;">
				<div class="search-form">
				<?php $this->renderPartial('_search',array(
					'model'=>$model,
				)); ?>
				</div>
			</div>
   		</div>
	</div>	
</div> <!-- search-form -->
<?php endif;?>

<a href="<?php echo Yii::app()->createUrl('project/create')?>"><?php echo Yii::t('common', 'new_project')?></a>

<?php /* 
 * В гриде CSS-классы бутстрапа вынес в конфиг приложения.
 * */ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'project-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=> $columns,
)); ?>