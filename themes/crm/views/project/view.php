<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
		Yii::t('common', 'manage_projects') => array('admin'),
		$model->name,
);
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1><?php echo Yii::t('common', 'view_project')." ".$model->name; ?></h1>
		
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		                    'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
		)); ?>
	</div>
</div>

<?php 
	function formatProjectStatus($types, $statuses){
		$html = '<table class="table table-bordered project-status-table">
					<thead>
						<tr>';
		
		foreach($types as $k=>$v)
			$html .= '<th>'.$v.'</th>';
		
		$html .=		'</tr>
					</thead>
					<tbody>
					    <tr>';
		foreach($types as $k=>$v){
			$html .= '<td style="background-color:'.(isset($statuses[$k]) ? $statuses[$k]['color'] : ProjectStatus::DEFAULT_COLOR).';">'.(isset($statuses[$k]) ? $statuses[$k]['status_name'] : '').'</td>';
		}
				
        $html .=  		'</tr>
        			</tbody>
             	</table>';
		
		return $html;
	}

?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'name',
		'owner.name',
		'create_date',
		'sign_date',
		'end_date',
		array(
				'label'=>'',
				'type' => 'html',
				'value'=> formatProjectStatus(ProjectType::getTypesText(), $model->getProjetStatuses()),
		),
	),
)); ?>
<style>
	table.detail-view .project-status-table th {text-align:center;}
	.project-status-table td{color: #fff; text-align:center; font-weight:bold;}
</style>