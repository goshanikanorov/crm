<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */
?>


<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins"> 
			<div class="ibox-title">
				<p class="note"><?php echo Yii::t('common', 'fields_with')?> <span class="required">*</span> <?php echo Yii::t('common', 'are_required')?>.</p>
			</div>                   
        	<div class="ibox-content">
        		
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'project-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation'=>false,
					'htmlOptions' => array('class'=> 'form-horizontal')
								)); ?>
					<?php if($form->errorSummary($model) != ""):?><div class="alert alert-danger"><?php echo $form->errorSummary($model) ?></div><?php endif;?>
                    
                     <div class="form-group">
                     	 <?php echo $form->labelEx($model, 'name', array('class'=>'col-lg-2 control-label')); ?>
                         <div class="col-lg-10">
	                         <?php echo $form->textField($model,'name', array('size'=>50,'maxlength'=>50, 'class'=> 'form-control')); ?>
                         </div>
                     </div>
                     <div class="form-group">
                         <?php echo $form->labelEx($model, 'end_date', array('class'=>'col-lg-2 control-label')); ?>
                         <div class="col-lg-10">
                            <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'end_date',    
                                'value'=>date('d/m/Y'),
                                'options'=>array(        
                                    'showButtonPanel'=>true,
                                    'dateFormat'=>'mm/dd/yy',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                                ),
                                'htmlOptions'=>array(
                                    'style'=>''
                                ),
                            ));
                            ?>
                         </div>
                     </div>
                     <div class="form-group">
                         <?php echo $form->labelEx($model, 'sign_date', array('class'=>'col-lg-2 control-label')); ?>
                         <div class="col-lg-10">
                            <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'sign_date',    
                                'value'=>date('d/m/Y'),
                                'options'=>array(        
                                    'showButtonPanel'=>true,
                                    'dateFormat'=>'mm/dd/yy',//Date format 'mm/dd/yy','yy-mm-dd','d M, y','d MM, y','DD, d MM, yy'
                                ),
                                'htmlOptions'=>array(
                                    'style'=>''
                                ),
                            ));
                            ?>
                         </div>
                     </div>
                     <table class="table table-bordered">
                            <thead>
                            <tr>
                                <?php foreach($types as $k=>$v):?>
                                	<th><?php echo $v?></th>
                                <?php endforeach;?>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            	<?php foreach($types as $k=>$v):?>
                                <td>
                                	<?php echo $form->dropDownList($model, 'status['.$k.']', ProjectStatus::model()->getStatusText(), array('class'=> 'form-control', 'empty'=>'')); ?>
                                </td>
                                <?php endforeach;?>
                            </tr>
                            </tbody>
                        </table>
                                
                     <div class="form-group">
                         <div class="col-lg-offset-2 col-lg-10">
                             <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('admin', 'create') : Yii::t('admin', 'save'), array('class' => 'btn btn-sm btn-white')); ?>
                         </div>
                     </div>	        			
				<?php $this->endWidget(); ?>
			</div>
   		</div>
	</div>	
</div>
<style>
	#project-form label.error{margin-left:0;}				
</style>