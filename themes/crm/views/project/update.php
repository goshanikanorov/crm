<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
		Yii::t('common', 'manage_projects') => array('admin'),
		$model->name=>array('view','id'=>$model->id),
		Yii::t('common', 'update'),
);
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<h1><?php echo Yii::t('common', 'update_project')." ".$model->name?></h1>
		
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		        'links'=>$this->breadcrumbs,
		        'homeLink'=>CHtml::link(Yii::t('common', 'home'),'/' ),
		)); ?>
	</div>
</div>	

<div class="wrapper wrapper-content animated fadeInRight">
	<?php $this->renderPartial('_form', array('model'=>$model, 'types'=>$types)); ?>
</div>