<div class="middle-box text-center loginscreen  animated fadeInDown">
	 <div>
        <div>
            <h1 class="logo-name">IN+</h1>
        </div>
            <h3>Welcome to IN+</h3>
            <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Login in. To see it in action.</p>
			<div class="form-group has-error">
			<?php if(!empty($model->errors)) 
			foreach($model->errors as $error)
				echo "<span class='control-label'>".$error."</span></br>"			
			?></div>		   
            <form class="m-t" role="form" method="post" action="?r=site/login">
                <div class="form-group">                   
				<?php echo CHtml::activeTextField($model, 'login', array('class'=>'form-control', 'placeholder'=> 'Логин', 'required'=>true)); ?>
                </div>
                <div class="form-group">
                <?php echo CHtml::activePasswordField($model, 'password', array('class'=>'form-control', 'placeholder'=> 'Пароль', 'required'=>true)); ?>          
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b"><?php echo Yii::t('common', 'enter');?></button>
                <!-- <a href="#"><small>Забыли пароль?</small></a>
                <p class="text-muted text-center"><small><?php /*echo Yii::t('common', 'dont_have_an_account');*/?></small></p>
                <a class="btn btn-sm btn-white btn-block" href="?r=site/register"><?php /*echo Yii::t('common', 'register');*/?></a>-->
            </form>	
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
       </div>	    
</div>
