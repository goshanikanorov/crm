<div class="row">
	<div class="col-md-12">
		<div class="middle-box text-center loginscreen   animated fadeInDown">
            <form class="m-t" method="post" role="form" action="?r=site/register">
                <div class="form-group">                   
				<?php echo CHtml::activeTextField($model, 'login', array('class'=>'form-control', 'placeholder'=> 'Логин', 'required'=>true)); ?>
                </div>
                <div class="form-group">
                <?php echo CHtml::activePasswordField($model, 'password', array('class'=>'form-control', 'placeholder'=> 'Пароль', 'required'=>true)); ?>          
                </div>
               
                <div class="form-group">                   
				<?php echo CHtml::activeTextField($model, 'name', array('class'=>'form-control', 'placeholder'=> 'Имя', 'required'=>true)); ?>
                </div>
                <div class="form-group">                   
				<?php echo CHtml::activeTextField($model, 'surname', array('class'=>'form-control', 'placeholder'=> 'Фамилия', 'required'=>true)); ?>
                </div>
                <div class="form-group">                   
				<?php echo CHtml::activeTextField($model, 'middlename', array('class'=>'form-control', 'placeholder'=> 'Отчество')); ?>
                </div>
                <div class="form-group">                   
				<?php echo CHtml::activeEmailField($model, 'email', array('class'=>'form-control', 'placeholder'=> 'E-mail', 'required'=>true)); ?>
                </div>
                <div class="form-group">                   
				<?php echo CHtml::activeTelField($model, 'phone', array('class'=>'form-control', 'placeholder'=> 'Телефон', 'data-mask'=>'+7(999) 999-9999')); ?>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Зарегистрироваться</button>
           		<p class="text-muted text-center"><small>Уже зарегистрированы?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="?r=site/login">Войти</a>
            </form>
		</div>
		<script src="<?=Yii::app()->theme->baseUrl?>/js/plugins/jasny/jasny-bootstrap.min.js"></script>
	</div>
</div>