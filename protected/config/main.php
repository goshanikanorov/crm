<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'crm',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.controllers.*',
        'application.components.*',
        'application.modules.user.*',
        'application.modules.user.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'mypass',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => false,
        ),
    ),
    // application components
    'components' => array(
        /**
         * RBAC
         */
        'authManager' => array(
            'class' => 'PhpAuthManager',
            'defaultRoles' => array('guest'),
        ),
        'user' => array(
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'yexcel' => array(
            'class' => 'ext.yexcel.Yexcel'
        ),
        'allpositions' => array(
            'class' => 'ext.allpositions.Allpositions'
        ),
        /**
         * Настройка виджетов из коробки под бутстрап
         */
        'widgetFactory' => array(
            'widgets' => array(
                'CBaseListView' => array(
                    'pagerCssClass' => '',
                ),
                'CLinkPager' => array(
                    'cssFile' => false,
                    'htmlOptions' => array(
                        'class' => 'pagination',
                    ),
                    'prevPageLabel' => '<span class="fa fa-angle-left"></span>',
                    'nextPageLabel' => '<span class="fa fa-angle-right"></span>',
                    'firstPageLabel' => '<span class="fa fa-angle-double-left"></span>',
                    'lastPageLabel' => '<span class="fa fa-angle-double-right"></span>',
                    'internalPageCssClass' => 'paginate_button',
                    'hiddenPageCssClass' => 'disabled',
                    'header' => '',
                    'selectedPageCssClass' => 'active',
                ),
                'CGridView' => array(
                    'itemsCssClass' => 'table table-striped',
                ),
            ),
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */

        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        /**
         * Отключал дебаггер браузера зачем-то
         */
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, trace, profile, info',
                ),
            // uncomment the following to show log messages on web pages
//				array(
//					'class'=>'CWebLogRoute',
//				),
            ),
        ),
    // 'authManager'=>array(
    //           'class'=>'CPhpAuthManager',
    //           // 'connectionID'=>'db',
    //       ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
    'theme' => 'crm',
    'sourceLanguage' => 'en_US',
    'language' => 'ru',
        //'charset'=>'utf-8',
);
