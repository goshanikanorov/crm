<?php

/**
 * Тут у нас все роли расписаны
 */

return array(
    User::ROLE_GUEST => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => Yii::t('admin', 'role_guest'),
        'bizRule' => null,
        'data' => null,
    ),
    User::ROLE_SEO => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => Yii::t('admin', 'role_seo'),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null,
    ),
    User::ROLE_CLIENT => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => Yii::t('admin', 'role_client'),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null,
    ),
    User::ROLE_PROGRAMMER => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => Yii::t('admin', 'role_programmer'),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null,
    ),
    User::ROLE_MANAGER => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => Yii::t('admin', 'role_manager'),
        'children' => array(
            User::ROLE_GUEST,
        ),
        'bizRule' => null,
        'data' => null,
    ),
    User::ROLE_ADMIN => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => Yii::t('admin', 'role_admin'),
        'children' => array(
            User::ROLE_SEO,
            User::ROLE_MANAGER,
            User::ROLE_PROGRAMMER,
            User::ROLE_CLIENT,
        ),
        'bizRule' => null,
        'data' => null,
    ),
);
