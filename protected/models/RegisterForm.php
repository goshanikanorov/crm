<?php

class RegisterForm extends CFormModel
{
	public $login;
	public $password;
	//public $password2;
	public $email;
	public $name;
	public $surname;
	public $middlename;
	public $phone;

	public function rules()
	{
		return array(
			array('login, password, email, name, surname', 'required'),
			array('login', 'length', 'max'=>10),
			array('phone', 'length', 'max'=>20),
			array('email, name, surname, middlename', 'length', 'max'=>50),
			array('login, password1, email, name, surname', 'length', 'min'=>3),
		);
	}
}

