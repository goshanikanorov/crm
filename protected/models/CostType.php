<?php
/**
 * This is the model class for table "crm_cost_type".
 *
 * The followings are the available columns in table 'crm_cost_type':
 * @property integer $id
 * @property string $type_name
 * @property string $type_pf
 */
class CostType extends CActiveRecord
{
	const TYPE_INCOME = 0;
	const TYPE_OUTCOME = 1;
	const TYPE_OVERHEAD = 2;
	const TYPE_OVERHEAD_INCOME = 3;

	private $_parent;
	private $_parentName;

	public function afterFind()
	{
		//$this->initParent($this->projectTypes);
		//echo "<pre>";
		//связь CostType и ProjectType уникальная, поэтому всегда 1 цикл
		foreach ($this->projectTypes as $pt) {
			$this->initParent($pt->id);
		}
	}

	public function initParent($projectTypeId)
	{
		$parent = Yii::app()->db->createCommand();

		$row = $parent->select('parent')
			->from('crm_cost_type_project_type')
			->where('project_type_id = '.$projectTypeId.' AND cost_type_id = '.$this->id)
			->queryRow();

		$this->_parent=$row['parent'];

	}

	public function getParent()
	{
		return $this->_parent;
	}

	public function setParent($id)
	{
		$this->_parent = $id;
	}

	public function getParentName()
	{
		if ($this->_parent)
		{
			$parent = self::model()->findByPk($this->_parent);
			$this->_parentName = $parent->type_name;
			return $this->_parentName;
		}
	}


	public function tableName()
	{
		return 'crm_cost_type';
	}
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function relations()
	{
		return array(
				'id' => array(self::HAS_MANY, 'Cost', 'type'),
				'projectTypes' => array(self::MANY_MANY, 'ProjectType', 'crm_cost_type_project_type(cost_type_id, project_type_id)')
		);
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type_name', 'required'),
			array('type_name, type_pf, parent', 'safe')
		);
	}

	public function getListType(){
		$status_list = self::findAll();
		$res = array();
		foreach($status_list as $status){
			$res[$status->attributes['id']] = $status->attributes['type_name'];
		}
		return $res;
	}

	/**
	 * Получить список типов для "Статей". Могут быть расход или приход.
	 * @return array массив возможных типов
	 */
	public function getCostOptions()
	{
		return array(
			self::TYPE_INCOME => 'Приход',
			self::TYPE_OUTCOME => 'Расход',
			self::TYPE_OVERHEAD => 'Накладные',
			self::TYPE_OVERHEAD_INCOME => 'Накладные приход'
		);
	}

	public function getCostOptionLabel()
	{
		$options = $this->getCostOptions();
		return $options[$this->type_pf];
	}

	/*
	 * todo переписать:
	 * $parent = self::model()->findByPk($id);
	 * return $parent ? $parent->name : null;
	 */
	/*public function getParentName()
	{
		return $this->_parent;
		if ($this->_parent != '') {
			$parentName = self::findByPk($this->_parent);
			return $parentName=$parentName->attributes['type_name'];
		}


	}*/



}