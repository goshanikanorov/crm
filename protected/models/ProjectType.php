<?php

/**
 * This is the model class for table "crm_project_type".
 *
 * The followings are the available columns in table 'crm_project_type':
 * @property integer $id
 * @property string $name
 * @property string $alias
 */
class ProjectType extends CActiveRecord
{
	public function tableName()
	{
		return 'crm_project_type';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>128),
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'costTypes' => array(self::MANY_MANY, 'CostType', 'crm_cost_type_project_type(project_type_id, cost_type_id)')
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название договора',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTypesText(){
		$result=array();
		$types =self::model()->findAll(array('order'=>'id'));
		foreach($types as $type){
			$result[$type->attributes['id']] = $type->attributes['name'];
		}
		return $result;
	}

	public function behaviors(){
	        return array('ESaveRelatedBehavior' => array(
	         'class' => 'application.components.ESaveRelatedBehavior')
	     );
	}
}
