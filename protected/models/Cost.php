<?php

/**
 * This is the model class for table "crm_project".
 *
 * The followings are the available columns in table 'crm_project':
 * @property integer $id
 * @property string $name
 * @property integer $owner_id
 * @property string $create_date
 */
class Cost extends CActiveRecord
{

	const XLS_PATH = 'data/xls';

	//id статей для импорта
	const COST_INCOME_ALL = 100;
	const COST_OUTCOME_DEV = 101;
	const COST_OUTCOME_SUPPORT = 102;
	const COST_OUTCOME_SEO = 103;
	const COST_OUTCOME_ADV = 104;
	const COST_OVERHEAD_SALARY = 105;
	const COST_OVERHEAD_SEO = 106;
	const COST_OVERHEAD_HOZ = 107;
	const COST_OVERHEAD_OTHER = 108;
	const COST_OVERHEAD_DEV = 109;
	const COST_OVERHEAD_HOZ_INCOME = 110;
	const COST_OVERHEAD_DIVIDENTS = 111;
	const COST_TRANSACTIONS_INCOME = 112;
	const COST_TRANSACTIONS_OUTCOME = 113;
	//const COST_TAX_INCOME = 114;
	const COST_TAX_OUTCOME = 114;

	//id типы проектов для импорта
	const PROJECT_TYPE_DEV = 1;
	const PROJECT_TYPE_SUPPORT = 2;
	const PROJECT_TYPE_SEO = 3;
	const PROJECT_TYPE_ADV = 4;

	//Типы статей (план/факт)
	const COST_PLANNED = 0;
	const COST_FACT = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_cost';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('sum', 'required'),
			// array('comment, user_id, article, project_id, date, sum, type', 'required'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_id' => Yii::t('common', 'project'),
			'type' => Yii::t('common', 'type'),
			'user_id' => Yii::t('common', 'user'),
			'article' => Yii::t('common', 'article'),
			'sum' => Yii::t('common', 'sum'),
			'comment' => Yii::t('common', 'comment'),
			'date' => Yii::t('common', 'date'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
				'Project' => array(self::HAS_ONE, 'Project', array('project_id'=>'id'), 'through'=>'ProjectToType'),
				'ProjectToType' => array(self::BELONGS_TO, 'ProjectToType', 'project_to_type_id'),
				//'Type' => array(self::BELONGS_TO, 'CostType', 'type'),
				'User' => array(self::BELONGS_TO, 'User', 'user_id'),
				//'Article' => array(self::BELONGS_TO, 'CostArticle', 'article'),
				'CostType' => array(self::BELONGS_TO, 'CostType', 'cost_type_id'),
				'PaymentAccount' => array(self::BELONGS_TO, 'PaymentAccount', 'payment_account_id')
		);
	}

	public function getAllCost(){		
		$costs = Yii::app()->db->createCommand()->select('*')
				->from($this->tableName().' c')
				->join(CostArticle::model()->tableName().' ca', 'c.article=ca.id')
				->join(CostType::model()->tableName().' ct', 'c.type=ct.id')->queryAll();
		// print_r($costs);
		// $result = array();
		// foreach($project_types as $type)
		// 	$result[$type['type_id']] = $type;
		return $costs;
	}

	public function getHtJSON($params=array())
	{
		//print_r($params);
		//die();
		$items=array();
		$arr_params=array();
		$sql='';

		$conditions['project'] = 'Project';
		$condition = '';

		if (count($params)!=0)
		{
			if ($params['manager']!='') $condition .= 'user_id IN ('.implode(',', $params['manager']).')';
			$condition !='' ? $and = ' AND ' : $and = '';
			if ($params['type']!='') $condition .= $and.'cost_type_id IN ('.implode(',', $params['type']).')';

			if (is_array($params['project']))
			{
				$conditions['project'] = array('Project'=>array(
						'joinType'=>'LEFT JOIN',
						'condition'=>'Project.id IN ('.implode(',',$params['project']).')'
				));
			}


			if ($params['item']!='')
			{
				// echo 4;
				// $criteria->compare('user_id', $params['manager'][0]);
				$or='';
				if ($sql!='') $sql.=' AND ';
				$sql.='(';
				foreach ($params['item'] as $kid => $id)
				{
					$sql.=$or.'Article.id=:article_id'.$kid;
					$or=' OR ';
					$arr_params[':article_id'.$kid]=$id;
				}
				$sql.=')';
			}
			//if ($sql!='') $sql.=' AND ';

			$condition !='' ? $and = ' AND ' : $and = '';
			$condition .= $and."date BETWEEN '".date('Y-m-d H:i:s',strtotime($params['datestart']))."' AND '
			".date('Y-m-d H:i:s',strtotime($params['dateend']))."'";
		}
		// print_r($arr_params);
		// echo $sql;
		//todo: переделать, чтобы связь была через ProjectType
		//$data=Cost::model()->with('User')->with('ProjectType')->with('Type')->with('Article')->findAll($sql,$arr_params);
		//$data=Cost::model()->with('User')->with('ProjectType')->with('CostType')->with('Article')->findAll();
		$data=Cost::model()->with('User')
				->with($conditions['project'])
				->with('CostType')->findAll($condition);
		//echo "<pre>";
		foreach ($data as $key => $value)
		{

			//print_r($value);

			//if (isset($value->Project->name)) echo $value->Project->name;
			$item=array();
			$item[0]=$value->id;

			if (isset($value->User->login)) $item[1]=$value->User->login;
			else $item[1]='';

			//todo переделать дату
			$item[2]=$value->date;

			if ($value->CostType->type_pf == CostType::TYPE_OUTCOME || $value->CostType->type_pf == CostType::TYPE_OVERHEAD)
				$item[3]=intval($value->sum);
			else $item[3] = '';

			if ($value->CostType->type_pf == CostType::TYPE_INCOME)
				$item[4]=intval($value->sum);
			else $item[4] = '';

			if (isset($value->Project->name)) $item[5]=$value->Project->name;
			else $item[5]='';
			if (isset($value->CostType->type_name)) $item[6]=$value->CostType->type_name;
			else $item[6]='';
			if (isset($value->type)) $item[7]=$this->getCostName($value->type);
			else $item[7]='';

			$item[8] = $value->PaymentAccount->name;

			$item[9]=$value->comment;

			$items[]=$item;

			//print_r($item);
			//die();
			unset($item);
		}
		if (count($items)==0) $items=array(array('','','','','','',''));
		return json_encode($items);
	}

	public function getJSON($type)
	{
		if ($type=='manager') $arr=$this->getManagers();
		elseif ($type=='projects') $arr=$this->getProjects();
		elseif ($type=='types') $arr=$this->getTypes();
		elseif ($type=='articles') $arr=$this->getArticles();
		foreach ($arr as $item)
		{
			$items[]=$item;
		}
		return json_encode($items);
	}

	public function getManagers()
	{
		$user=new User();
		foreach ($user->findAll() as $key => $value)
		{
			$out[$value->id]=$value->login;
		}
		return $out;
	}

	public function getProjects()
	{
		$project=new Project();
		foreach ($project->findAll() as $key => $value)
		{
			$out[$value->id]=$value->name;
		}
		return $out;
	}

	public function getTypes()
	{
		$type=new CostType();
		foreach ($type->findAll() as $key => $value)
		{
			$out[$value->id]=$value->type_name;
		}
		return $out;
	}

	public function getArticles()
	{
		$article=new CostArticle();
		foreach ($article->findAll() as $key => $value)
		{
			$out[$value->id]=$value->article_name;
		}
		return $out;
	}
	
	public function SaveChanges($params)
	{
		 //print_r($params);
		// echo 1;
		if ($params['data'][0][4]!='')
		{
			// echo 4;
			$model=Cost::model()->findByPk($params['data'][0][4]);
			switch ($params['data'][0][1]) {
				case 1: 
					$model_user=User::model()->find('login=:login',array(':login'=>$params['data'][0][3]));
					$model->user_id=$model_user->id;
					break;
				case 2: $model->date=date('Y-m-d H:i:s',strtotime($params['data'][0][3]));
					break;
				case 3: $model->sum=$params['data'][0][3];
					break;
				case 4: $model->sum_comming=$params['data'][0][3];
					break;
				case 5: 
					$model_project=Project::model()->find('name=:name',array(':name'=>$params['data'][0][3]));
					$model->project_id=$model_project->id;
					break;
				case 6: 
					$model_type=CostType::model()->find('type_name=:type_name',array(':type_name'=>$params['data'][0][3]));
					$model->type=$model_type->id;
					break;
				case 7: 
					$model_article=CostArticle::model()->find('article_name=:article_name',array(':article_name'=>$params['data'][0][3]));
					$model->article=$model_article->id;
					break;
				case 8: $model->comment=$params['data'][0][3];
					break;
				default:
					break;
			}
		}
		else
		{
			// echo 123;
			$model=new Cost();
			if ($params['data'][0][1]==5)
			{
				$model_project=Project::model()->find('name=:name',array(':name'=>$params['data'][0][3]));
				$model->project_id=$model_project->id;
			}
			else $model->project_id=0;
			if ($params['data'][0][1]==1)
			{
				$model_user=User::model()->find('login=:login',array(':login'=>$params['data'][0][3]));
				$model->user_id=$model_user->id;
			}
			else $model->user_id=0;
			if ($params['data'][0][1]==6)
			{
				$model_type=CostType::model()->find('type_name=:type_name',array(':type_name'=>$params['data'][0][3]));
				$model->type=$model_type->id;
			}
			else $model->type=0;
			if ($params['data'][0][1]==7)
			{
				$model_article=CostArticle::model()->find('article_name=:article_name',array(':article_name'=>$params['data'][0][3]));
				$model->article=$model_article->id;
			}
			else $model->article=0;
			$model->sum=($params['data'][0][1]==3?$params['data'][0][3]:0);
			$model->comment=($params['data'][0][1]==8?$params['data'][0][3]:0);
			$model->date=($params['data'][0][1]==2?date('Y-m-d H:i:s',strtotime($params['data'][0][3])):date('Y-m-d H:i:s'));;
			// print_r($model);
		}
		$model->save();
		if ($params['data'][0][4]=='') echo $this->getHtJSON();
		return true;
	}


	public static function getXlsPath() {
		return Yii::app()->basePath . '/' . self::XLS_PATH;
	}

	public static function getExcelMimeTypes() {
		return array(
			'application/vnd.ms-excel',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			//'.csv',
		);
	}

	public static function getXlsStructure() {
		return array(
			'B' => 'user',
			'C' => 'date',
			'E' => 'expenses',
			'F' => 'income',
			'G' => 'debt',
			'H' => 'comment',
			'I' => 'client',
			'J' => 'type'
		);
	}

	public static function importXls()
	{
		Yii::app()->db->createCommand()->truncateTable(self::tableName());
		$upload = CUploadedFile::getInstanceByName('FlatExcel');
		if (array_search($upload->getType(), self::getExcelMimeTypes()) === false) {
			return 'Неверный тип файла';
		}
		$fileName = date("d.m.Y") . "_" . md5(time()) . "." . $upload->getExtensionName();
		$filePath = self::getXlsPath() . '/' . $fileName;
		if ($upload->saveAs($filePath)) {
			$sheetArray = Yii::app()->yexcel->readActiveSheet($filePath);

			if ($sheetArray === false) {
				return 'Файлы CSV не поддерживаются';
			}

			$indexes = self::getXlsStructure();
			$rows = array();
			$buildings = array();

			//echo "<pre>";
			foreach ($sheetArray as $key => $row) {
				if ($key > 3) {

					//echo "<pre>";
					//print_r($row);
					echo $key." - ";
					$account = $row['B'];
					$date = $row['C'];
					$expenses = $row['E'];
					$income = $row['F'];
					$plan_outcome = $row['G'];
					$plan_income = $row['H'];
					$comment = $row['I'];
					$client = $row['J'];
					$type = $row['K'];

					$cost = new Cost;

					$record = PaymentAccount::model()->findByAttributes(array('name'=>$account));
					if ($record === NULL){echo "NO account"; die;}
					else $cost->payment_account_id = $record->id ;

					//echo "<pre>";
					//echo $cost->PaymentAccount->id;
					//die();

					$ndate = DateTime::createFromFormat('m-d-y',$date);
					$cost->date = date('Y-m-d H:i:s',$ndate->getTimestamp());
					echo 'ГДЕ';
					//echo $date;
					//echo $cost->date;
					//continue;

					/*$cost->sum = $expenses;
					$cost->sum_comming = $income;*/


					/* todo ptt_id - project_to_type_id. Нужно узнать, есть ли проект
					сначала делаем меппинг $type в файле на project_type в базе, после чего
					1 - если не найден проект - создаем с таким названием
					2 - когда найден или создан проект, ищем договор
					3 - создаем договор ПРОЕКТ - ТИП по полю type и привязываем
					4 - договор найден - добавляем его ID
					5 - договор не найден - добавляем его ID и привязываем
					*/

					//если поле проект (клиент) пустое, значит это накладной расход или приход по ХОЗ ДЕЯТЕЛЬНОСТИ
					$costIsOverhead = false; //флаг, что затрата накладная
					if (trim($client) == '')
					{
						echo "Накладные - ";
						$costIsOverhead = true;
						//if ($expenses > 0) $costType = self::COST_;
						//elseif ($income > 0) $costType = self::DEF_COST_INCOME_OVERHEAD;
					}
					else {
						$project = Project::model()->findByAttributes(array('name'=>$client));

						if ($project === NULL) {
							$newProject = new Project();
							$newProject->name = $client;
							$newProject->owner_id = 1;
							$newProject->save();
							$clientId = $newProject->id;
						}
						else $clientId = $project->id;
						//echo "<h1>".$clientId."</h1>";
						echo "Прямые - ".$client." - ";
					}

					$cost->type = self::COST_FACT;
					//Проверяем, какой тип проекта/затраты (в excel перемешано)
					//todo сделать DEBT



					switch ($type) {
						case 'разработка':
							//echo $expenses.' -- '.$income.' -- '. $debt.' : '.$costTypeId.'|';
							$projectTypeId = self::PROJECT_TYPE_DEV;
							if ($expenses>0) $costTypeId = self::COST_OUTCOME_DEV;
							elseif ($income>0) $costTypeId = self::COST_INCOME_ALL;
							elseif ($plan_income > 0) {
								$costTypeId = self::COST_INCOME_ALL;
								$cost->type = self::COST_PLANNED;
							}
							elseif ($plan_outcome > 0) {
								$costTypeId = self::COST_OUTCOME_DEV;
								$cost->type = self::COST_PLANNED;
							}
							else {print_r($row); die("NO SUM");};
							break;
						case 'поддержка':
							echo $type;
							$projectTypeId = self::PROJECT_TYPE_SUPPORT;
							if ($expenses > 0) {
								$costTypeId = self::COST_OUTCOME_SUPPORT;
							}
							elseif ($income > 0) {
								$costTypeId = self::COST_INCOME_ALL;
							}
							elseif ($plan_income > 0) {
								$costTypeId = self::COST_INCOME_ALL;
								$cost->type = self::COST_PLANNED;
							}
							elseif ($plan_outcome > 0) {
								$costTypeId = self::COST_OUTCOME_SUPPORT;
								$cost->type = self::COST_PLANNED;
							}
							else {print_r($row); die("NO SUM");};
							break;
						case 'продвижение':
							$projectTypeId = self::PROJECT_TYPE_SEO;
							if ($expenses > 0) $costTypeId = self::COST_OUTCOME_SEO;
							elseif ($income > 0) $costTypeId = self::COST_INCOME_ALL;
							elseif ($plan_income > 0) {
								$costTypeId = self::COST_INCOME_ALL;
								$cost->type = self::COST_PLANNED;
							}
							elseif ($plan_outcome > 0) {
								$costTypeId = self::COST_OUTCOME_SEO;
								$cost->type = self::COST_PLANNED;
							}
							else {print_r($row); die("NO SUM");};
							break;
						case 'реклама':
							$projectTypeId = self::PROJECT_TYPE_ADV;
							if ($expenses > 0) $costTypeId = self::COST_OUTCOME_ADV;
							elseif ($income > 0) $costTypeId = self::COST_INCOME_ALL;
							elseif ($plan_income > 0) {
								$costTypeId = self::COST_INCOME_ALL;
								$cost->type = self::COST_PLANNED;
							}
							elseif ($plan_outcome > 0) {
								$costTypeId = self::COST_OUTCOME_ADV;
								$cost->type = self::COST_PLANNED;
							}
							else {print_r($row); die("NO SUM");};
							break;
						case 'хоз деят':
							$projectTypeId = NULL;

							//todo сделать для PLANNED
							if ($expenses > 0) $costTypeId = self::COST_OVERHEAD_HOZ;
							elseif ($income > 0) $costTypeId = self::COST_OVERHEAD_HOZ_INCOME;
							//todo раскидывать по типам
							$costIsOverhead = true;
							break;
						case 'зарплата':
							$projectTypeId = NULL;
							//todo сделать для PLANNED

							$costTypeId = self::COST_OVERHEAD_SALARY;
							$costIsOverhead = true;
							break;
						case 'дивиденты':
							$projectTypeId = NULL;
							$costTypeId = self::COST_OVERHEAD_DIVIDENTS;
							$costIsOverhead = true;
							break;
						case 'транзакции':
							$projectTypeId = NULL;
							if ($expenses > 0) $costTypeId = self::COST_TRANSACTIONS_OUTCOME;
							elseif ($income > 0) $costTypeId = self::COST_TRANSACTIONS_INCOME;
							$costIsOverhead = true;
							break;
						case 'налоги':
							$projectTypeId = NULL;
							if ($expenses > 0) $costTypeId = self::COST_TAX_OUTCOME;
							elseif ($income > 0) $costTypeId = self::COST_TAX_INCOME;
							$costIsOverhead = true;
							break;
						default:
							die("EMPTY СТАТЬЯ");
					}


					//если затрата накладная, не нужно создавать проект, определяем статью и записываем в базу
					//если затрата не накладная, есть проект - то создаем проект
					//todo сделать для OVERHEAD INCOM проекты
					if (!$costIsOverhead) {
						echo 'НЕ НАКЛАДНЫЕ';
						//ищем если уже есть такой проект
						$projectToType = ProjectToType::model()->findByAttributes(array('project_id' => $clientId, 'project_type_id'=>$projectTypeId));


						if ($projectToType === NULL) {


							$projectToType = new ProjectToType();
							$projectToType->project_type_id = $projectTypeId;
							$projectToType->project_id = $clientId;
							if ($projectToType->save()) ;//die("сохраним");
							else {
								print_r($client);
								print_r($projectToType->getErrors());
								print_r($row);
								(die("cant save"));
							}
							$projectToTypeId = $projectToType->id;
						}
						else $projectToTypeId = $projectToType->id;
					}
					else {
						if ($costTypeId == self::COST_OUTCOME_SEO) $costTypeId = self::COST_OVERHEAD_SEO;
						if ($costTypeId == self::COST_OUTCOME_DEV) $costTypeId = self::COST_OVERHEAD_DEV;
						$projectToTypeId = 0;
					}

					//die($projectToTypeId);

					//$cost = new Cost;
					$cost->User = 1;
					$cost->comment = $comment;
					$cost->project_to_type_id = $projectToTypeId;
					$cost->cost_type_id = $costTypeId;
					if ($expenses > 0) {
						$cost->sum = $expenses;
						$cost->type = self::COST_FACT;
					}
					elseif ($income > 0) {
						$cost->sum = $income;
						$cost->type = self::COST_FACT;
					}
					elseif ($plan_outcome > 0 ) {
						$cost->sum = $plan_outcome;
						$cost->type = self::COST_PLANNED;
					}
					elseif ($plan_income > 0 ) {
						$cost->sum = $plan_income;
						$cost->type = self::COST_PLANNED;
					}
					//$cost->sum = $expenses > 0 ? $expenses : $income;

					$cost->sum_comming = $income;


					//echo "$costTypeId -- ";
					//echo "$projectToTypeId";




					if ($cost->save()) echo "COOL<br>";
					else {
						echo "can't save<br>";
						print_r($cost->getErrors());
						die();
						//echo $cost->errors();
					}


					//if ($expenses > 0) $costType = self::DEF_COST_OUTCOME;
					//elseif ($income > 0) $costType = self::DEF_COST_INCOME;

					//$cost->project_to_type_id = $ptt_id;


					//die();


					/*foreach ($row as $index => $column) {
						switch ($index) {
							case 'A':
								$building = $value = str_replace(self::PREFIX_BUILDING, '', $column);
								break;
							case 'B':
								$value = str_replace(self::PREFIX_FLOOR, '', $column);
								break;
							case 'H':
								$value = self::getStatusByLabel($column);
								break;
							default:
								$value = $column;
						}
						$rows[$key][$indexes[$index]] = $value;
					}
					$buildings[$building][] = $rows[$key]['flat_number'];
					*/
				}
			}

			/*foreach ($buildings as $key => $flats) {
				$criteria = new CDbCriteria();
				$criteria->addInCondition('flat_number', $flats);
				$criteria->addCondition('building = ' . $key);
				self::model()->deleteAll($criteria);
			}

			$builder = Yii::app()->db->schema->commandBuilder;
			$command = $builder->createMultipleInsertCommand(self::model()->tableName(), $rows);
			$command->execute();*/

			return true;
		}
		return 'Ошибка при загрузке файла';
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		// $post=Cost::model()->findByPk(1);
		// echo $post->article_name.' '.$post->article_description;
		// die();
		// // Получаем автора записи. Здесь будет выполнен реляционный запрос.
		// print_r($post);
		// // $author=$post->type_name;
		// die();

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('project_id',$this->project_id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('user_id', $this->user_id,true);
		$criteria->compare('article', $this->article,true);
		$criteria->compare('sum', $this->sum,true);
		$criteria->compare('comment', $this->comment,true);
		$criteria->compare('date', $this->date,true);
		$criteria->compare('article_name', CostArticle::model()->article_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getCostTypes(){
		return array(
			self::COST_PLANNED => 'План',
			self::COST_FACT => 'Факт'
		);
	}

	public function getCostName($id)
	{
		$names = $this->getCostTypes();
		return $names[$id];
	}
}
