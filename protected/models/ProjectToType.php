<?php

/**
 * This is the model class for table "crm_project_to_type".
 *
 * The followings are the available columns in table 'crm_project_to_type':
 * @property integer $id
 * @property integer $project_type_id
 * @property integer $project_id
 * @property integer $project_status_id
 * @property string $last_changed_date
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property ProjectType $projectType
 * @property ProjectStatus $projectStatus
 */
class ProjectToType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_project_to_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('project_type_id, project_id', 'required'),
			//array('project_type_id, project_id, project_status_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, project_type_id, project_id, project_status_id, last_changed_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'costs' => array(self::HAS_MANY,'Cost', 'project_to_type_id'),
			'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
			'projectType' => array(self::BELONGS_TO, 'ProjectType', 'project_type_id'),
			'projectStatus' => array(self::BELONGS_TO, 'ProjectStatus', 'project_status_id'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'project_type_id' => 'Project Type',
			'project_id' => 'Project',
			'project_status_id' => 'Project Status',
			'last_changed_date' => 'Last Changed Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('project_type_id',$this->project_type_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('project_status_id',$this->project_status_id);
		$criteria->compare('last_changed_date',$this->last_changed_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProjectToType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getBudgetJSON($year)
	{
		$budget = array();
		$sumIncome = array();
		$sumOutcome = array();

		$countCostTypes = array('income'=>0, 'outcome'=>0);
		foreach ($this->projectType->costTypes as $costType) {
			if ($costType->type_pf == 0) $countCostTypes['income']++;
			elseif ($costType->type_pf == 1 || $costType->type_pf == 2) $countCostTypes['outcome']++;
			$budget[$costType->id][1] = $costType->type_name;
		}


		//$data=Cost::model()->with('ProjectType')->with('Type')->with('Article')->findAll();
		$costs = $this->costs(array('condition'=>'year(date) = '.$year));
		foreach ($costs as $cost) {
			$i = $cost->CostType->id;


			$month = Yii::app()->dateFormatter->format('M', $cost->date);
			//для таблицы бюджета, чтобы шли ровно
			$month+=1;

			isset($budget[$i][$month]) ? $budget[$i][$month] += $cost->sum : $budget[$i][$month] = $cost->sum;

			//суммарный факт
			isset($budget[$i][14]) ? $budget[$i][14] += $cost->sum : $budget[$i][14] = $cost->sum;

			//суммарно по месяцам
			if ($cost->CostType->type_pf == 0)
				isset($sumIncome[$month]) ? $sumIncome[$month]+=$cost->sum : $sumIncome[$month]= $cost->sum;

			if ($cost->CostType->type_pf == 1)
				isset($sumOutcome[$month]) ? $sumOutcome[$month]+=$cost->sum : $sumOutcome[$month]= $cost->sum;



			//суммируем для родительских todo передалать под http://www.yiiframework.com/wiki/61/
			$parent = 1;
			$recurCost = $cost;
			while ($parent)
			{
				if ($recurCost->CostType->parent > 0)
				{
					isset($budget[$recurCost->CostType->parent][$month]) ? $budget[$recurCost->CostType->parent][$month]
						+= $recurCost->sum : $budget[$recurCost->CostType->parent][$month] = $recurCost->sum;
					$parentId = CostType::model()->findByPk($recurCost->CostType->parent);
					$recurCost->CostType->parent = $parentId->parent;
				}
				else $parent = 0;
				//$parent = 0;
			}
		}

		$margin[14]=0;
		foreach ($sumIncome as $key=>$val)
		{
			isset($margin[$key]) ? $margin[$key] += $val : $margin[$key] = $val;
			$margin[14] +=$val;
		}

		foreach ($sumOutcome as $key=>$val)
		{
			isset($margin[$key]) ? $margin[$key] -= $val : $margin[$key] = -$val;
			$margin[14] -=$val;
		}


		$sumOutcome = array(0=>'',1=>'ИТОГО РАСХОДОВ') + $sumOutcome;
		$sumIncome = array(0=>'',1=>'ИТОГО ВЫРУЧКА') + $sumIncome;
		$margin = array(0=>'', 1=>'МАРЖИНАЛЬНЫЙ ДОХОД') + $margin;

		//var_dump($countCostTypes);

		//echo "<pre>";

		$budgetTMP = array();
		foreach ($budget as $key => $val)
		{
			$budgetTMP[] = array($key)+$val;
		}
		//print_r($budgetTMP);
		//array_splice( $budgetTMP, $countCostTypes['income'], 0, array(array(1=>'ИТОГО ВЫРУЧКА'), array(''), array(1=>'РАСХОДЫ')));
		array_splice( $budgetTMP, $countCostTypes['income'], 0, array($sumIncome, array(''), array(1=>'РАСХОДЫ')));
		array_splice( $budgetTMP, $countCostTypes['outcome']+$countCostTypes['income']+3, 0, array($sumOutcome, array(''),$margin));
		//echo "<pre>";
		//var_dump($budgetTMP);
		//die();
		$budgetJSON = CJSON::encode($budgetTMP);
		return $budgetJSON;
		//print_r($budgetJSON);
	}

	public static function getDDSJSON($year)
	{
		$dds = array();
		$ddsTMP = array();
		$ddsMargin = array();
		$ddsTotal = array();
		$ddsOverhead = array();
		$ddsOverheadTotal = array();
		$ddsGlobalTotal = array();
		$ddsPlannedNotPayed = array();
 		$dds[0] = array('Статьи', 'ЯНВАРЬ', 'ФЕВРАЛЬ', 'МАРТ', 'АПРЕЛЬ','МАЙ', 'ИЮНЬ','ИЮЛЬ','АВГУСТ',	'СЕНТЯБРЬ', 'ОКТЯБРЬ', 'НОЯБРЬ', 'ДЕКАБРЬ', 'ИТОГО 2015 ФАКТ', 'ИТОГО 2015 ПЛАН','ОТКЛОНЕНИЕ');
		$dds[1] = array('Доходная часть');

		if ($year === NULL) $year = date("Y");

		$projectTypes = ProjectType::model()->findAll();

		foreach ($projectTypes as $projectType)
		{
			//echo '<pre>';
			//echo $projectType->name;
			//$ddsTMP[$projectType->id][0] = $projectType->name;
			$ddsTMP[$projectType->id][0] = array();
		}

		$costs = Cost::model()->findAll(array('condition'=>'year(date) = '.$year));

		foreach ($costs as $cost)
		{
			$month = Yii::app()->dateFormatter->format('M', $cost->date) + 1;
			$costType = $cost->CostType->type_pf;

			if (isset($cost->ProjectToType)) {
				$projectTypeId = $cost->ProjectToType->projectType->id;
				$projectTypeName = $cost->ProjectToType->projectType->name;


				if ($costType == CostType::TYPE_INCOME)
				{
					$ddsTMP[$projectTypeId][$costType][1] = 'ВЫРУЧКА ДОГОВОРОВ '.$projectTypeName;

					isset($ddsTMP[$projectTypeId][$costType][$month]) ?
							$ddsTMP[$projectTypeId][$costType][$month]+=$cost->sum :
							$ddsTMP[$projectTypeId][$costType][$month] = $cost->sum;

					isset($ddsTMP[$projectTypeId][$costType][14]) ?
							$ddsTMP[$projectTypeId][$costType][14]+=$cost->sum :
							$ddsTMP[$projectTypeId][$costType][14] = $cost->sum;

					isset($ddsMargin[$projectTypeId][$month]) ?
							$ddsMargin[$projectTypeId][$month] += $cost->sum :
							$ddsMargin[$projectTypeId][$month] = $cost->sum;

					isset($ddsMargin[$projectTypeId][14]) ?
							$ddsMargin[$projectTypeId][14] += $cost->sum :
							$ddsMargin[$projectTypeId][14] = $cost->sum;


					isset($ddsTotal['income'][$month]) ? $ddsTotal['income'][$month] += $cost->sum : $ddsTotal['income'][$month] = $cost->sum;
					isset($ddsTotal['income'][14]) ? $ddsTotal['income'][14] += $cost->sum : $ddsTotal['income'][14] = $cost->sum;

					isset($ddsTotal['margin'][$month]) ? $ddsTotal['margin'][$month] += $cost->sum : $ddsTotal['margin'][$month] = $cost->sum;
					isset($ddsTotal['margin'][14]) ? $ddsTotal['margin'][14] += $cost->sum : $ddsTotal['margin'][14] = $cost->sum;


				}
				elseif ($costType == CostType::TYPE_OUTCOME) {
					$ddsTMP[$projectTypeId][$costType][1] = 'ЗАТРАТЫ ДОГОВОРОВ '.$projectTypeName;

					isset($ddsTMP[$projectTypeId][$costType][$month]) ?
							$ddsTMP[$projectTypeId][$costType][$month]-=$cost->sum :
							$ddsTMP[$projectTypeId][$costType][$month] = -$cost->sum;

					isset($ddsTMP[$projectTypeId][$costType][14]) ?
												$ddsTMP[$projectTypeId][$costType][14]-=$cost->sum :
												$ddsTMP[$projectTypeId][$costType][14] = -$cost->sum;

					isset($ddsMargin[$projectTypeId][$month]) ?
							$ddsMargin[$projectTypeId][$month] -= $cost->sum :
							$ddsMargin[$projectTypeId][$month] = -$cost->sum;

					isset($ddsMargin[$projectTypeId][14]) ?
							$ddsMargin[$projectTypeId][14] -= $cost->sum :
							$ddsMargin[$projectTypeId][14] = -$cost->sum;

					isset($ddsTotal['outcome'][$month]) ? $ddsTotal['outcome'][$month] -= $cost->sum : $ddsTotal['outcome'][$month] = -$cost->sum;
					isset($ddsTotal['margin'][$month]) ? $ddsTotal['margin'][$month] -= $cost->sum : $ddsTotal['margin'][$month] = -$cost->sum;

					isset($ddsTotal['outcome'][14]) ? $ddsTotal['outcome'][14] -= $cost->sum : $ddsTotal['outcome'][14] = -$cost->sum;
					isset($ddsTotal['margin'][14]) ? $ddsTotal['margin'][14] -= $cost->sum : $ddsTotal['margin'][14] = -$cost->sum;
				}
				//elseif ($costType == CostType::TYPE_OVERHEAD)

					//$ddsTMP[$projectTypeId][$costType][0] = 'НАКЛАДНЫЕ ДОГОВОРОВ '.$projectTypeName;






				//var_dump($cost->ProjectToType);
				//$ddsTMP[$cost->ProjectToType->projectType->id][$month] = $cost->sum;
			}
			else {

				if ($costType == CostType::TYPE_OVERHEAD)
				{
				//todo накладные
				isset($ddsOverhead[$cost->CostType->type_name][$month]) ?
						$ddsOverhead[$cost->CostType->type_name][$month] -= $cost->sum :
						$ddsOverhead[$cost->CostType->type_name][$month] = -$cost->sum;

				isset($ddsOverhead[$cost->CostType->type_name][14]) ?
						$ddsOverhead[$cost->CostType->type_name][14] -= $cost->sum :
						$ddsOverhead[$cost->CostType->type_name][14] = -$cost->sum;

				isset($ddsOverheadTotal[$month]) ? $ddsOverheadTotal[$month] -= $cost->sum :
						$ddsOverheadTotal[$month] = -$cost->sum;

				isset($ddsOverheadTotal[14]) ? $ddsOverheadTotal[14] -= $cost->sum :
										$ddsOverheadTotal[14] = -$cost->sum;
				}
				elseif ($costType == CostType::TYPE_OVERHEAD_INCOME)
				{
					isset($ddsOverhead[$cost->CostType->type_name][$month]) ?
							$ddsOverhead[$cost->CostType->type_name][$month] += $cost->sum :
							$ddsOverhead[$cost->CostType->type_name][$month] = $cost->sum;

					isset($ddsOverhead[$cost->CostType->type_name][14]) ?
							$ddsOverhead[$cost->CostType->type_name][14] += $cost->sum :
							$ddsOverhead[$cost->CostType->type_name][14] = $cost->sum;

					isset($ddsOverheadTotal[$month]) ? $ddsOverheadTotal[$month] += $cost->sum :
							$ddsOverheadTotal[$month] = $cost->sum;

					isset($ddsOverheadTotal[14]) ? $ddsOverheadTotal[14] += $cost->sum :
							$ddsOverheadTotal[14] = $cost->sum;
				}
			}

			//todo продумать как сделать
			if ($costType == CostType::TYPE_INCOME || $costType == CostType::TYPE_OVERHEAD_INCOME)
			{
				isset($ddsGlobalTotal[$month]) ?
						$ddsGlobalTotal[$month] += $cost->sum :
						$ddsGlobalTotal[$month] = $cost->sum;
				isset($ddsGlobalTotal[14]) ? $ddsGlobalTotal[14] += $cost->sum :
									$ddsGlobalTotal[14] = $cost->sum;
			}
			elseif ($costType == CostType::TYPE_OUTCOME || $costType == CostType::TYPE_OVERHEAD)
			{
				isset($ddsGlobalTotal[$month]) ?
						$ddsGlobalTotal[$month] -= $cost->sum :
						$ddsGlobalTotal[$month] = -$cost->sum;
				isset($ddsGlobalTotal[14]) ? $ddsGlobalTotal[14] -= $cost->sum :
									$ddsGlobalTotal[14] = -$cost->sum;
			}



			if ($cost->type == Cost::COST_PLANNED)
			{
				//die("PLANNED");
				if ($costType == CostType::TYPE_INCOME || $costType == CostType::TYPE_OVERHEAD_INCOME)
					isset($ddsPlannedNotPayed['income'][$month]) ?
							$ddsPlannedNotPayed['income'][$month] += $cost->sum :
							$ddsPlannedNotPayed['income'][$month] = $cost->sum;
				elseif ($costType == CostType::TYPE_OUTCOME || $costType == CostType::TYPE_OVERHEAD)
					isset($ddsPlannedNotPayed['outcome'][$month]) ?
							$ddsPlannedNotPayed['outcome'][$month] -= $cost->sum :
							$ddsPlannedNotPayed['outcome'][$month] = -$cost->sum;
			}

		}
		//echo "<pre>";
		//print_r($ddsTMP);
		//die();
		//print_r($ddsMargin);
		///echo "<pre>";
		//print_r($ddsTotal);
		$ddsFinal[] = array(1=>'Доходная часть');
		foreach ($ddsTMP as $projectTypeId => $val)
		{
			$ddsFinal[] = $val[CostType::TYPE_INCOME];
			$ddsFinal[] = $val[CostType::TYPE_OUTCOME];
			$ddsFinal[] = array(1=>'МАРЖИНАЛЬНЫЙ ДОХОД') + $ddsMargin[$projectTypeId];
			$ddsFinal[] = array();

		}

		$ddsFinal[] =  array(1=>'ВЫРУЧКА') + $ddsTotal['income'];
		$ddsFinal[] =  array(1=>'ПРЯМЫЕ ЗАТРАТЫ') + $ddsTotal['outcome'];
		$ddsFinal[] =  array(1=>'МАРЖИНАЛЬНЫЙ ДОХОД') + $ddsTotal['margin'];
		$ddsFinal[] =  array();
		$ddsFinal[] =  array(1=>'Накладные расходы');
		//print_r($ddsOverhead);
		foreach ($ddsOverhead as $key => $val)
		{
			$ddsFinal[] = array(1=>$key) + $val;

		}

		$ddsFinal[] = array(1=>'ИТОГО НАКЛАДНЫЕ РАСХОДЫ') + $ddsOverheadTotal;
		$ddsFinal[] = array();

		$ddsFinal[] = array(1=>'НЕОПЛАЧЕННЫЙ ПЛАН ПРИХОД') + $ddsPlannedNotPayed['income'];
		//$ddsFinal[] = array(1=>'НЕОПЛАЧЕННЫЙ ПЛАН РАСХОД') + $ddsPlannedNotPayed['outcome'];


		$ddsFinal[] = array(1=>'ЧИСТАЯ ПРИБЫЛЬ') + $ddsGlobalTotal;
		$ddsJSON = CJSON::encode($ddsFinal);
				return $ddsJSON;
		//print_r($ddsGlobalTotal);
		//die();
	}
}
