<?php

/**
 * This is the model class for table "crm_user".
 *
 * The followings are the available columns in table 'crm_user':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $name
 * @property string $surname
 * @property string $middlename
 * @property string $role
 * @property string $phone
 * @property string $create_date
 */
class User extends CActiveRecord
{
	const ROLE_ADMIN = 'admin';
	const ROLE_PROGRAMMER = 'programmer';
	const ROLE_MANAGER = 'manager';
	const ROLE_CLIENT = 'client';
	const ROLE_SEO = 'seo';
        /**
         * Роль гость как базовая роль
         */
        const ROLE_GUEST = 'guest';
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, email, name, surname, role', 'required'),
			array('password', 'required', 'on'=>'insert'),
			array('login, password, phone', 'length', 'max'=>20),			
			array('name, surname, middlename, email', 'length', 'max'=>50),
			//array('create_date, role', 'safe'),
			array('login, email, name, surname, middlename, role, phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
				'projects' => array(self::HAS_MANY, 'Project', 'owner_id'),
				'cost' => array(self::HAS_MANY, 'Cost', 'user_id'),
				'Seo' => array(self::MANY_MANY, 'Seo', 'crm_su(user_id, project_to_type_id)')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => Yii::t('admin', 'login'),
			'password' => Yii::t('admin', 'password'),
			'email' => Yii::t('admin', 'email'),
			'name' => Yii::t('admin', 'name'),
			'surname' => Yii::t('admin', 'surname'),
			'middlename' => Yii::t('admin', 'middlename'),
			'role' => Yii::t('admin', 'role'),
			'phone' => Yii::t('admin', 'phone'),
			'create_date' => Yii::t('admin', 'create_date'),
			'projects' => Yii::t('common', 'projects'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('middlename',$this->middlename,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('create_date',$this->create_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=> 20,
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function beforeSave()
	{
		if($this->isNewRecord) $this->create_date = date("Y-m-d H:i:s");
	
		if($this->role == null)
			$this->role = User::ROLE_CLIENT;
		
		if(trim($this->password) != ""){
			$this->password = $this->hashPassword($this->password);
		} else {
			
		}	
		return parent::beforeSave();
	}
	
	public function validatePassword($password)
	{
		return CPasswordHelper::verifyPassword($password,$this->password);
	}
	
	public function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password);
	}
	
	/**
	 * Все роли
	 *
	 * @return array
	 */
	public static function getRoles()
	{
		return array(		
			self::ROLE_CLIENT => Yii::t('admin', 'role_client'),
			self::ROLE_MANAGER =>  Yii::t('admin', 'role_manager'),
			self::ROLE_PROGRAMMER =>  Yii::t('admin', 'role_programmer'),
			self::ROLE_ADMIN => Yii::t('admin', 'role_admin'),
			self::ROLE_SEO => Yii::t('admin', 'role_seo'),
		);
	}
	
	public function getRolesText(){
		$roles = self::getRoles();
		return $roles[$this->role];	
	}
	
	public function userAlreadyExists(){
		
		$user = Yii::app()->db->createCommand()
		->select('login')
		->from($this->tableName())
		->where('login=:login', array(':login'=> $this->login))
		->queryRow();
		
		return $user;
	}
		
	public function emailAlreadyExists(){
	
		$email = Yii::app()->db->createCommand()
		->select('email')
		->from($this->tableName())
		->where('email=:email', array(':email'=> $this->email))
		->queryRow();
	
		return $email;
	}

	public function getListUser(){
		$status_list = self::findAll();
		$res = array();
		foreach($status_list as $status){
			$res[$status->attributes['id']] = $status->attributes['login'];
		}
		return $res;
	}
}
