<?php

/**
 * Вроде ничего не поменял
 */

/**
 * This is the model class for table "crm_user".
 *
 * The followings are the available columns in table 'crm_user':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $name
 * @property string $surname
 * @property string $middlename
 * @property string $role
 * @property string $phone
 * @property string $create_date
 */
class Seo extends ProjectToType {

    //Для теста
    public $project_info = '';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'crm_project_to_type';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                // array('login, email, name, surname, role', 'required'),
                // array('password', 'required', 'on'=>'insert'),
                // array('login, password, phone', 'length', 'max'=>20),			
                // array('name, surname, middlename, email', 'length', 'max'=>50),
                //array('create_date, role', 'safe'),
                // array('login, email, name, surname, middlename, role, phone', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'User' => array(self::MANY_MANY, 'User', 'crm_su(project_to_type_id, user_id)')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    // public function attributeLabels()
    // {
    // 	return array(
    // 		'id' => 'ID',
    // 		'login' => Yii::t('admin', 'login'),
    // 		'password' => Yii::t('admin', 'password'),
    // 		'email' => Yii::t('admin', 'email'),
    // 		'name' => Yii::t('admin', 'name'),
    // 		'surname' => Yii::t('admin', 'surname'),
    // 		'middlename' => Yii::t('admin', 'middlename'),
    // 		'role' => Yii::t('admin', 'role'),
    // 		'phone' => Yii::t('admin', 'phone'),
    // 		'create_date' => Yii::t('admin', 'create_date'),
    // 		'projects' => Yii::t('common', 'projects'),
    // 	);
    // }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('middlename', $this->middlename, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('create_date', $this->create_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        ));
    }

    public function returnAllPosition() {
        $companies = Yii::app()->allpositions->get_projects();
        return $companies;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
