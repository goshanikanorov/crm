<?php
class CostArticle extends CActiveRecord
{
	
	public function tableName()
	{
		return 'crm_cost_article';
	}
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function relations()
	{
		return array(
				'id' => array(self::HAS_MANY, 'Cost', 'article'),
		);
	}

	public function getListArticle(){
		$status_list = self::findAll();
		$res = array();
		foreach($status_list as $status){
			$res[$status->attributes['id']] = $status->attributes['article_name'];
		}
		return $res;
	}}