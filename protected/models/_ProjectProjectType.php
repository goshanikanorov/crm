<?php
class ProjectProjectType extends CActiveRecord
{
	
	public function tableName()
	{
		return 'crm_project_to_type';
	}
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function relations()
	{
		return array(
				'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
				'type'=> array(self::BELONGS_TO, 'ProjectType', 'project_type_id'),
				'status' => array(self::BELONGS_TO, 'ProjectStatus', 'project_status_id'),
				'costs'=> array(self::HAS_MANY, 'CostType', 'id')
				//'Cost' => array(self::BELONGS_TO, 'Cost', 'project_id'),
		);
	}
}