<?php

class LoginForm extends CFormModel {

    public $login;
    public $password;
    public $errors = array();
    private $_identity;

    public function rules() {
        return array(
            array('login, password', 'required'),
            array('login, password', 'safe'),
        );
    }

    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->login, $this->password);
            $this->_identity->authenticate();
        }

        /*
         * Добавил запоминание юзера на месяц
         */

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = 60 * 60 * 24 * 30;
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        } else {
            if ($this->_identity->errorCode === UserIdentity::ERROR_USERNAME_INVALID) {
                $this->errors[] = Yii::t('errors', 'user_does_not_exists');
            } elseif ($this->_identity->errorCode === UserIdentity::ERROR_PASSWORD_INVALID) {
                $this->errors[] = Yii::t('errors', 'wrong_password');
            }
            return false;
        }
    }

}
