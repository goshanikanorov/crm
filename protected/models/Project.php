<?php

/**
 * This is the model class for table "crm_project".
 *
 * The followings are the available columns in table 'crm_project':
 * @property integer $id
 * @property string $name
 * @property integer $owner_id
 * @property string $create_date
 */
class Project extends CActiveRecord
{
	public $status = array();

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'crm_project';
	}
	
	public function projectToTypeTableName(){
		return 'crm_project_to_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('owner_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('create_date, sign_date, end_date', 'safe'),
			array('name, owner_id, create_date, sign_date, end_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
				'owner' => array(self::BELONGS_TO, 'User', 'owner_id'),
				'projectTypes' => array(self::HAS_MANY, 'ProjectProjectType', 'project_id'),
				'Cost' => array(self::HAS_MANY, 'Cost', 'project_id'),
		);
	}
	
	public function getProjetStatuses(){		
		$project_types = Yii::app()->db->createCommand()->select('pt.id as type_id, ps.id as status_is, ptt.last_changed_date, ps.name as status_name, ps.color')
				->from($this->projectToTypeTableName().' ptt')
				->join(ProjectType::model()-> tableName().' pt', 'pt.id=ptt.project_type_id')
				->join(ProjectStatus::model()-> tableName().' ps', 'ps.id=ptt.project_status_id')
				->where('ptt.project_id=:id', array(':id'=>$this->id))->order('pt.id ASC')->queryAll();
		
		$result = array();
		foreach($project_types as $type)
			$result[$type['type_id']] = $type;
		return $result;
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('common', 'name'),
			'owner' => Yii::t('common', 'owner'),
			'create_date' => Yii::t('common', 'create_date'),
			'sign_date' => Yii::t('common', 'sign_date'),
			'end_date' => Yii::t('common', 'end_date'),
			'status' => ''
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('owner',$this->owner);
		$criteria->compare('create_date', $this->create_date,true);
		$criteria->compare('sign_date', $this->sign_date,true);
		$criteria->compare('end_date', $this->end_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function afterSave() {
		parent::afterSave();
		/*if($this->isNewRecord){
			// если мы создаем нового пользователя, тогда нам необходимо создать
			// для него запись в таблице профиля с ссылкой на родительскую таблицу
			$user_profile = new UserProfile;
			$user_profile->user_id =     $this->id;
			$user_profile->name =        $this->name;
			$user_profile->first_name =  $this->first_name;
			$user_profile->description = $this->description;
			$user_profile->save();
		} else {*/
			// иначе неободимо обновить данные в таблице профиля
			
			foreach($this->status as $type=>$status){
				
				// TODO: delete empty, move updated to history
				
				if(!empty($status)){
				$date = new DateTime();
				
				$is_record_exists = ProjectProjectType::model()->findByAttributes(
				    array('project_id'=> $this->id, 'project_type_id' => $type,)
				);
				
				if($is_record_exists){
					ProjectProjectType::model()->updateAll(
						array(
							'project_status_id' => $status,
							'last_changed_date'=>$date->format('Y-m-d H:i:s'),					
						), 
						'project_id=:project_id and project_type_id=:project_type_id',
						array(':project_id'=> $this->id, 'project_type_id'=>$type)
					);	
				} else {		
					$project_project_type = new ProjectProjectType();
					$project_project_type->project_id = $this->id;
					$project_project_type->project_type_id = $type;
					$project_project_type->project_status_id = $status;
					$project_project_type->last_changed_date = $date->format('Y-m-d H:i:s');
					
					$project_project_type->save();
				}			
			}
		}	
	//	}
	}
	
	public function beforeDelete(){		
		ProjectProjectType::model()->deleteAll("project_id=:project_id", array(":project_id"=>$this->id));
		return parent::beforeDelete();
	}

	public function getListProject(){
		$status_list = self::findAll();
		$res = array();
		foreach($status_list as $status){
			$res[$status->attributes['id']] = $status->attributes['name'];
		}
		return $res;
	}
}
