<?php
return array(
		'user_does_not_exists' => 'Пользователь с таким логином не существует',
		'wrong_password' => 'Неверный пароль',
		'user_already_exists' => 'Пользователь с таким логином уже существует',
		'email_already_exists' => 'Пользователь с таким E-mail уже существует'
);
?>