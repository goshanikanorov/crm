<?php

class CostController extends ClientController {
    
    /**
     * Тут все так же, как в CostController2
     * Не очень понял в чем между ними разница)))
     */

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /* public function accessRules()
      {
      return array(
      array('allow',  // allow all users to perform 'index' and 'view' actions
      'actions'=>array('index','view'),
      'users'=>array('*'),
      ),
      array('allow', // allow authenticated user to perform 'create' and 'update' actions
      'actions'=>array('create','update'),
      'users'=>array('@'),
      ),
      array('allow', // allow admin user to perform 'admin' and 'delete' actions
      'actions'=>array('admin','delete'),
      'users'=>array('admin'),
      ),
      array('deny',  // deny all users
      'users'=>array('*'),
      ),
      );
      } */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Project;
        print_r($_POST);
        die();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Project'])) {
            $data = $_POST['Project'];
            $data['owner_id'] = Yii::app()->user->getId();
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['sign_date'] = date('Y-m-d H:i:s', strtotime($_POST['sign_date']));
            $data['end_date'] = date('Y-m-d H:i:s', strtotime($_POST['end_date']));
            $model->attributes = $data;
            $model->status = $_POST['Project']['status'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
            'types' => ProjectType::getTypesText(),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate() {
        $model = new Cost();
        $model->SaveChanges($_POST);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Cost();
        // die();
        // print_r($_POST);
        if (isset($_POST['Cost'])) {
            $model->user_id = $_POST['Cost']['user_id'];
            $model->article = $_POST['Cost']['article'];
            $model->project_id = $_POST['Cost']['project_id'];
            $model->type = $_POST['Cost']['type'];
            $model->sum = $_POST['Cost']['sum'];
            $model->comment = $_POST['Cost']['comment'];
            $model->date = date('Y-m-d H:i:s', strtotime(preg_replace('/\//isu', '.', $_POST['end_date'])));
            $model->save();
        }

        $columns = array(
            array(
                'name' => 'sum',
                'value' => 'CHtml::link($data->sum, "#",array("onclick"=>"select_grid_item($(this));","onblur"=>"console.log(\'diselect\');"))',
                'type' => 'raw'
            ),
            'sum',
            'comment',
            'date',
            'Article.article_name',
            'Article.article_description',
            'Type.type_name',
            'Project.name',
            array(
                'class' => 'CButtonColumn',
                'template' => '{delete}',
                'buttons' => array(
                    'delete' => array(
                        'click' => "function(){
			                $.ajax({
			                type:'POST',
			                url:$(this).attr('href'),
			                success:function(data) {
			                    $.fn.yiiGridView.update('cost-grid');
								$('#user-grid').yiiGridView('update', {
									data: $(this).serialize()
								});
			                }
			            })
			            return false;
			            }",
                    ),
                )
            ),
        );
        $model->unsetAttributes();
        $this->render('admin', array(
            'model' => $model,
            'columns' => $columns,
        ));
    }

    public function actionFilter() {
        $model = new Cost();
        $array = json_decode($model->getHtJSON($_POST), true);
        echo json_encode(array('data' => $array));
    }

    public function actionUpload() {
        $status = null;

        if (isset($_FILES['FlatExcel'])) {
            $status = Cost::importXls();
            if ($status === true) {
                //$this->redirect('cost/upload');
                Yii::app()->end();
            }
        }

        $this->render('upload', array(
            'status' => $status,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Project the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Cost::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        // $model->status = $model->getProjetStatuses();
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Project $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cost-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
