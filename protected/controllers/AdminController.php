<?php

// остальную админку наследовать от него
class AdminController extends Controller {
    
    /**
     * Тоже просто убрал пару кусков в components/Controller
     * Правило доступа надо переделать
     * 'roles' => array(User::ROLE_ADMIN),
     * Вместо
     * 'expression' => "Yii::app()->user->role=='admin'"
     */

    public function accessRules() {
        return array(
            array('allow',
                //'roles'=>array('admin')
                'expression' => "Yii::app()->user->role=='admin'"
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

}
