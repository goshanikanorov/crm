<?php

/*
 * Только автоформат
 */

class SiteController extends Controller {

    public $layout = 'site';

    public function actionIndex() {
        error_reporting(E_ALL | E_STRICT);
        ini_set('display_errors', 'On');
        if (Yii::app()->user->isGuest) {
            $this->redirect(array('site/login'));
        } else {
            if (Yii::app()->user->isAdmin()) {
                $this->redirect(array('admin/index'));
            } else {
                $this->redirect(array('client/index'));
            }
        }
    }

    public function actionLogin() {
        error_reporting(E_ALL | E_STRICT);
        ini_set('display_errors', 'On');
        $model = new LoginForm;

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            } else {
                $this->render('login', array('model' => $model));
            }
        }
        $this->layout = 'login';
        $this->render('login', array('model' => $model));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    /* 	public function actionRegister()
      {
      $model=new RegisterForm;

      if(isset($_POST['RegisterForm']))
      {
      $model->attributes = $_POST['RegisterForm'];
      $user = new User;
      $user->attributes = $model->attributes;

      $user->save();
      $errors = $user->getErrors();
      if(empty($errors)){
      if($user->login()){
      $this->redirect(Yii::app()->user->returnUrl);
      }
      }
      //if($model->validate() && $model->login())
      //$this->redirect(Yii::app()->user->returnUrl);
      }
      $this->render('register', array('model'=>$model));
      } */

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', array('error' => $error));
        }
    }

}
