<?php

class ProjectController extends ClientController {

    /**
     * Тут вроде ничего не менял, просто автоформат сделал, чтобы проще было прочитать
     */
    
    /**
     * Убрал фильтры, т.к. они в родительских классах объявлены
     */
    
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    /* public function accessRules()
      {
      return array(
      array('allow',  // allow all users to perform 'index' and 'view' actions
      'actions'=>array('index','view'),
      'users'=>array('*'),
      ),
      array('allow', // allow authenticated user to perform 'create' and 'update' actions
      'actions'=>array('create','update'),
      'users'=>array('@'),
      ),
      array('allow', // allow admin user to perform 'admin' and 'delete' actions
      'actions'=>array('admin','delete'),
      'users'=>array('admin'),
      ),
      array('deny',  // deny all users
      'users'=>array('*'),
      ),
      );
      } */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Project;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['Project'])) {
            $data = $_POST['Project'];
            $data['owner_id'] = Yii::app()->user->getId();
            $data['create_date'] = date('Y-m-d H:i:s');
            $data['sign_date'] = date('Y-m-d H:i:s', strtotime($_POST['sign_date']));
            $data['end_date'] = date('Y-m-d H:i:s', strtotime($_POST['end_date']));
            $model->attributes = $data;
            $model->status = $_POST['Project']['status'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'types' => ProjectType::getTypesText(),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        //$model = Project::model()->with('projectTypes', 'projectTypes.type', 'projectTypes.status')->findByPk($id);
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Project'])) {
            $model->attributes = $_POST['Project'];
            $model->status = $_POST['Project']['status'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'types' => ProjectType::getTypesText(),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        die('123');
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Project('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Project']))
            $model->attributes = $_GET['Project'];

        $columns = array(
            'name',
            array(
                'header' => Yii::t('common', 'owner'),
                'value' => '$data->owner->name',
            ),
            'create_date',
            'sign_date',
            'end_date',
            array(
                'class' => 'ButtonColumn',
                'buttons' => array(
                    'delete' => array(
                        'click' => "function(){
                        $.ajax({
                        type:'POST',
                        url:$(this).attr('href'),
                        success:function(data) {
                            $.fn.yiiGridView.update('project-grid');
							$('#user-grid').yiiGridView('update', {
								data: $(this).serialize()
							});
                        }
                    })
                    return false;
                    }",
                    ),
                )
            ),
        );

        if (!Yii::app()->user->isAdmin()) {
            unset($columns[1]);
        }

        $this->render('admin', array(
            'model' => $model,
            'columns' => $columns,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Project the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Project::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model->status = $model->getProjetStatuses();
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Project $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
