<?php

class SeoController extends ClientController {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array(
                /**
                 * Правила доступа через функцию контроллера - сам по себе плохой подход,
                 * Запрещать смотреть модель, не связанную с текущим юзером надо на уровне модели.
                 * В Yii есть такая вещь, scopes - что-то вроде фильтра фильтра, который будет на все выборки работать,
                 * Или только на определенные сценарии. Вот через него и надо разруливать эту логику.
                 * Т.е. когда мы вызываем loadModel - моделька сама должна посмотреть текущего юзера,
                 * И определить, можно ли ему выдать результат. Если не нельзя - в итоге пользователю выбросит 404 ошибку.
                 * Правда скоупы тогда не успел прикрутить, а потом из башки вылетело. Напомни плиз, сделаю.
                 * 
                 * Запилил здесь доступ к действию view только роль на SEO, т.к. админ владеет этими же правами.
                 */
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('view'),
                'roles' => array(
                    User::ROLE_SEO,
                ),
            //'users' => array(Yii::app()->user->name),
            // 'expression'=>'Yii::app()->user->role==\'seo\' || Yii::app()->user->role==\'admin\'',
            //'expression' => array($this, 'allowOnlyOwner'), //задние правил через функцию
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        // print_r(Yii::app()->getModule('user')->getAdmins();
        $model = $this->loadModel($id);
        $seo = new Seo();
        $this->render('view', array(
            'companies' => $seo->returnAllPosition(),
            'info' => CJSON::decode($model->project_info),
        ));
    }

    public function allowOnlyOwner($id) {
        $model = $this->loadModel($_GET['id']);
        if (Yii::app()->user->role == 'seo' || Yii::app()->user->role == 'admin') {
            if (isset($model->User[0]) && $model->User[0]->id == Yii::app()->user->id)
                return true;
            else
                return false;
        }
        else {
            return false;
        }
    }

    public function loadModel($id) {
        $model = Seo::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

}
