<?php

// остальную часть ичного кабинета клиента наследовать от него
class ClientController extends Controller {
    
    /**
     * Убрал в components/controllers нужные везде куски кода
     */

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

}
