<?php

class ProjectTypeController extends AdminController {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new ProjectType;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ProjectType'])) {
            $model->attributes = $_POST['ProjectType'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ProjectType'])) {

            $model->attributes = $_POST['ProjectType'];

            /**
             * В этом контроллере я только автоформат сделал
             * Здесь, по хорошему, надо все убрать, кроме $model->save()
             * В модель, в метод afterSave() например
             * Хотел попробовать но тоже че-то подзабил.
             */

            if (isset($_POST['costTypes']) && $model->validate()) {

                $costTypes = array();

                foreach ($_POST['costTypes'] as $key => $value) {
                    $costTypes[$key] = new CostType;
                    $costTypes[$key]->attributes = $value;
                    $costTypes[$key]->parent = $value['parent'];

                    if ($costTypes[$key]->validate())
                        $costTypes[$key]->save();
                    else
                        unset($costTypes[$key]);
                }

                $model->costTypes = $costTypes;
            }
            $model->save();

            $command = Yii::app()->db->createCommand();
            foreach ($costTypes as $key => $costType) {
                $command->insert('crm_cost_type_project_type', array(
                    'project_type_id' => $model->id,
                    'cost_type_id' => $costType->id,
                    'parent' => $costType->parent
                ));
            }
        }


        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('ProjectType');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new ProjectType('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ProjectType']))
            $model->attributes = $_GET['ProjectType'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ProjectType the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = ProjectType::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ProjectType $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'project-type-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
