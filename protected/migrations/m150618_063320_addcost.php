<?php

class m150618_063320_addcost extends CDbMigration
{
	public function up()
	{
		$this->createTable('crm_cost', array(
            'id' => 'pk',
            'project_id' => 'int(11) NOT NULL',
            'type' => 'int(11) NOT NULL',
            'user_id' => 'int(11) NOT NULL',
            'article' => 'int(11) NOT NULL',
            'sum' => 'int(11) NOT NULL',
            'comment' => 'text',
            'date' => 'datetime NOT NULL',
          ));
        $this->createIndex('project_id', 'crm_cost', 'project_id', false);
        $this->createIndex('type', 'crm_cost', 'type', false);
        $this->createIndex('user_id', 'crm_cost', 'user_id', false);
        $this->createIndex('article', 'crm_cost', 'article', false);

		$this->createTable('crm_cost_article', array(
            'id' => 'pk',
            'article_name' => 'text',
            'article_description' => 'text',
        ));

		$this->createTable('crm_cost_type', array(
            'id' => 'pk',
            'type_name' => 'text',
            'type_pf' => 'text',
        ));
	}

	public function down()
	{
		$this->dropTable('crm_cost');
		$this->dropTable('crm_cost_article');
		$this->dropTable('crm_cost_type');
		echo "m150618_063320_addcost does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}