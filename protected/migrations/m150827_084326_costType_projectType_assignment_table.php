<?php

class m150827_084326_costType_projectType_assignment_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('crm_cost_type_project_type', array(
	            'project_type_id' => 'int(11) NOT NULL',
	            'cost_type_id' => 'int(11) NOT NULL',
                'PRIMARY KEY (project_type_id, cost_type_id)',
        ));

        $this->addForeignKey('fk_cost_type_project_type', 'crm_cost_type_project_type', 'project_type_id',
            'crm_project_type', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_project_type_cost_type', 'crm_cost_type_project_type', 'cost_type_id',
            'crm_cost_type', 'id', 'CASCADE', 'RESTRICT');
	}

	public function down()
	{
        $this->dropTable('crm_cost_type_project_type');
		echo "m150827_084326_costType_projectType_assignment_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}