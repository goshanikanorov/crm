<?php

class m150617_063741_change_project extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE crm_project ADD sign_date DATETIME');
		$this->execute('ALTER TABLE crm_project ADD end_date DATETIME');
	}

	public function down()
	{
		$this->execute('ALTER TABLE crm_project DROP sign_date');
		$this->execute('ALTER TABLE crm_project DROP end_date');
		echo "m150617_063741_change_project does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}