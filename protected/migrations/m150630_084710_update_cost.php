<?php

class m150630_084710_update_cost extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE crm_cost ADD sum_comming INT(11)');
	}

	public function down()
	{
		$this->execute('ALTER TABLE crm_cost DROP sum_comming');
		echo "m150630_084710_update_cost does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}