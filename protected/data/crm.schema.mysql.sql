CREATE TABLE crm_user (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    login VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(128) NOT NULL,
    name VARCHAR(128) NOT NULL,
    surname VARCHAR(128) NOT NULL,
    middlename VARCHAR(255), 
    role VARCHAR(50) NOT NULL,
    phone VARCHAR(50),
    create_date DATETIME NOT NULL
);

CREATE TABLE crm_project (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  owner_id int(11) NOT NULL,
  create_date datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE crm_project_type (
  id int(2) NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO crm_project_type (`name`) VALUES ('разработка');
INSERT INTO crm_project_type (`name`)  VALUES ('поддержка');
INSERT INTO crm_project_type(`name`)  VALUES ('продвижение');
INSERT INTO crm_project_type (`name`) VALUES ('реклама');

CREATE TABLE crm_project_status (
  id int(2) NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL,
  color varchar(11) DEFAULT '#D8D8D8',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO crm_project_status (`name`, `color`) VALUES ('активен', '#1ab394');
INSERT INTO crm_project_status (`name`, `color`) VALUES ('завершен', '#ed5565');


CREATE TABLE crm_project_to_type (
  id int(11) NOT NULL AUTO_INCREMENT,
  project_id int(11) NOT NULL,
  project_type_id int(2) NOT NULL,
  project_status_id int(2) DEFAULT NULL,
  last_changed_date datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
