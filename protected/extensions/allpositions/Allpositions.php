<?

set_include_path(get_include_path() . PATH_SEPARATOR . Yii::app()->basePath . '/extensions/allpositions/');

include 'phpxmlrpc/lib/xmlrpc.inc';
include 'lib.php';

class Allpositions {

	private $api_key='42b97720cb1899510a4f5df39800de2c';
    function __construct() {

    }

    public function init() {
        
    }

    public function get_projects() {
    	$api = new xf3\AllPositions($this->api_key);
    	$projects = $api->get_projects();
    	if ($projects === null) {//При ошибке любая функция возвращает null
		    return null;
		} else {
			return $projects;
		}
    }

    public function get_visibility($project_id,$begin_date,$end_date,$id_se) {
    	$api = new xf3\AllPositions($this->api_key);
    	$visibility = $api->get_visibility($project_id,($begin_date==''?'null':date('Y-m-d',$begin_date)),($end_date==''?'null':date('Y-m-d',$end_date)),($id_se==''?'null':$id_se));
    	if ($visibility === null) {//При ошибке любая функция возвращает null
		    return null;
		} else {
			return $visibility;
		}
    }

}

// $test=new Allpositions();
// print_r($test->get_visibility(140281));

?>