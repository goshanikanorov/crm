<?php

set_include_path(get_include_path() . PATH_SEPARATOR . Yii::app()->basePath . '/extensions/yexcel/Classes/');
include 'PHPExcel/IOFactory.php';

class Yexcel {

    function __construct() {
        
    }

    public function init() {
        
    }

    public function readActiveSheet($file) {
        $fileType = PHPExcel_IOFactory::identify($file);

        if ($fileType === 'CSV') {
            return false;
        }

        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        return $sheetData;
    }

}
