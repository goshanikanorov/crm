<?php

/**
 * Автоформат
 */

class WebUser extends CWebUser {

    private $_model = null;

    private function getModel() {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = User::model()->findByAttributes(array("login" => Yii::app()->user->name));
        }
        return $this->_model;
    }

    public function isAdmin() {
        return $this->getRole() === 'admin';
    }

    public function getRole() {
        if ($user = $this->getModel()) {
            return $user->role;
        }
    }

    public function getId() {
        if ($user = $this->getModel()) {
            return $user->id;
        }
    }

}
